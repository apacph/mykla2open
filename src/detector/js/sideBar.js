/**
 * 開啟側邊欄
 */
function openNav() {
    document.getElementById("mySidebar").style.width = "22vw"; //設置側邊欄寬度為22vw
}

/**
 *  關閉側邊欄
 */
function closeNav() {
    document.getElementById("mySidebar").style.width = "0"; //設置側邊欄寬度為0
}