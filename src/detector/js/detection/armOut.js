// 記錄旋轉角度
let armOutRotation = {
    rightUpperY: 0,
    rightUpperZ: 0,
    rightLowerZ: 0,
    rightUpperZo: 0,

    leftUpperY: 0,
    leftUpperZ: 0,
    leftLowerZ: 0,
    leftUpperZo: 0
};

// 由mediapipe.js呼叫
// result為所有偵測資料，mult用來判斷左右邊
// mult等於一代表VRM的右邊、使用者的左邊、程式碼中寫left的部分，反之則為另一邊
function armOutDetection(results, mult) {
    if (results.poseLandmarks != undefined) {

        const leftShoulder = results.poseLandmarks[11];
        const leftElbow = results.poseLandmarks[13];
        //變數需先宣告，如果在下方if才宣告，若未跑進下方if時會出現錯誤(找不到變數)
        let leftWrist;
        let leftMidFin;
        if (results.leftHandLandmarks != undefined) {
            leftWrist = results.leftHandLandmarks[0];
            leftMidFin = results.leftHandLandmarks[9];
        }

        const rightShoulder = results.poseLandmarks[12];
        const rightElbow = results.poseLandmarks[14];
        let rightWrist; //在這邊定義 是為了因為下面有用到rightWrist 如果這裡rightHandLandmarks undefined的話 在裡面宣告的話 會沒進去 沒宣告到  ->下面要用到rightWrist的時候會錯
        let rightMidFin;
        if (results.rightHandLandmarks != undefined) {
            rightWrist = results.rightHandLandmarks[0]; //這邊程式碼wrist的部分是連過去handpose
            rightMidFin = results.rightHandLandmarks[9];
        }

        // let upperY;
        // let upperZ;
        // let lowerZ;
        // let upperZo;

        let shoulderRotation = getRotation(rightShoulder, leftShoulder); //肩膀的轉動角度(兩肩之間的角度)
        let armPart = [
            [rightShoulder, rightElbow, rightWrist, rightMidFin],
            [leftShoulder, leftElbow, leftWrist, leftMidFin]
        ]
        if(mult == 1){
            moveArmOut(armPart[0], mult, shoulderRotation)
        }
        else{
            moveArmOut(armPart[1], mult, shoulderRotation)
        }

    }
    else{   //若沒偵測到手，VRM手初始動作(手平舉姿勢)
        rotateArmOut(0, 0, 0, 0, mult);
    }
}

/**
 * 計算手臂旋轉角度
 * @param {*} arm 該手臂的值 0:肩膀 1:手肘 2:手腕 3:中指 4:大拇指 5:小指 
 * @param {*} mult 加權值  1:vrm右手 -1:vrm左手
 * @param {*} shoulderRotation 兩肩之間的角度
 */
 function moveArmOut(arm, mult, shoulderRotation) {
    
    if (arm[0].visibility >= 0.65 && arm[1].visibility >= 0.65 && arm[2]) { //當visibility >= 0.65 時在webcam的畫面上才會出現偵測點及線(有偵測到)

        // pointToVec = new THREE.Vector3 -> 都是將偵測點的資料轉為Vector3的形式，差別為pointToVec只須給部位就會將該部位的XYZ變成Vector3的形式，反之則需要給三個值才可轉成Vector3的形式
        // getAngle 需要以Vector3形式才可計算
        // XZ平面上，求上手臂Y的轉動角度
        let shoulderVecY = pointToVec(arm[0]) // 肩膀點
        let elbowVecY = new THREE.Vector3(arm[1].x, arm[0].y, arm[1].z) // 手肘X，肩膀Y，手肘Z
        let otherVecY = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z) //肩膀X左右延伸，肩膀Y，肩膀Z
        let upperY = getAngle(elbowVecY, shoulderVecY, otherVecY) * mult // 三點求角度


        if (arm[2].y <= arm[1].y && arm[3].z > -0.0035) { //手往後，手腕高於手肘
            upperY *= -0.1 * mult //mult使用者的右手有辦法往後彎 不然只能一直在前方擺動
        }

        if (arm[2].y > arm[1].y && arm[0].z - 0.01 < arm[1].z) { //手肘後於肩膀，手腕低於手肘
            upperY *= -1.5 
        }

        let upperZo = (getRotation(arm[1], arm[0]) - shoulderRotation); //openvtuber的寫法


        // XY平面上，求上手臂Z的轉動角度
        let shoulderVecZ = pointToVec(arm[0]) // 肩膀點
        let elbowVecZ = new THREE.Vector3(arm[1].x, arm[1].y, arm[0].z) // 手肘X，手肘Y，肩膀Z
        let otherVecZ = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z) // 肩膀X左右延伸，肩膀Y，肩膀Z
        let upperZ = getAngle(elbowVecZ, shoulderVecZ, otherVecZ) * mult // 三點求角度

        if ((arm[0].y < arm[1].y) * mult) { // 手肘低(高)於肩膀
            upperZ *= -1
        }

       
        //rightLowerZ openvtuber的寫法  -> https://github.com/voidedWarranties
        let lowerZ = getRotation(arm[2], arm[1]) - shoulderRotation;

        //手臂沒有舉起來 然後擺在身體前的狀況
        if(-(arm[0].x + 0.3 > arm[2].x) * mult && arm[1].y < arm[2].y){ // 手腕在肚子前，手腕低於手肘
            upperY *= 0.1 * mult
            upperZ *= 0.8
            // lowerZ *= 0.8
        }
        rotateArmOut(upperY, upperZ, lowerZ, upperZo, mult)
    } else {
        //若沒偵測到手
        // return[0,0,0,0];
        rotateArmOut(0, 0, 0, 0, mult)
    }
}

function rotateArmOut(upperY, upperZ, lowerZ, upperZo, mult) {
    //當沒有偵測到，手呈現初始姿勢
    if(upperY == 0 && upperZ == 0 && lowerZ == 0 && upperZo == 0){
        if (mult == 1) { //vrm的右手動畫
            vrmManager.rotation(Bone.RightUpperArm).y = 0;
            vrmManager.rotation(Bone.RightUpperArm).z = rad(-75);
            vrmManager.rotation(Bone.RightLowerArm).y = 0;
            vrmManager.rotation(Bone.RightLowerArm).z = 0;
        } else if (mult == -1) { //vrm的左手動畫
            vrmManager.rotation(Bone.LeftUpperArm).y = 0;
            vrmManager.rotation(Bone.LeftUpperArm).z = rad(75);
            vrmManager.rotation(Bone.LeftLowerArm).y = 0;
            vrmManager.rotation(Bone.LeftLowerArm).z = 0;
        }
    }
    else{
        //更新armOutRotation中的值
        if (mult == 1) {
            vrmManager.tween(armOutRotation, {
                rightUpperY: upperY,
                rightUpperZ: upperZ,
                rightLowerZ: lowerZ,
                rightUpperZo: upperZo
            }, () => updateArmRotation(armOutRotation, "right"), "rightArmOut", {
                rightUpperY: 0,
                rightUpperZ: rad(-75),
                rightLowerZ: rad(-85),
                rightUpperZo: upperZo,
            });
        } else if (mult == -1) {
            vrmManager.tween(armOutRotation, {
                leftUpperY: upperY,
                leftUpperZ: upperZ,
                leftLowerZ: lowerZ,
                leftUpperZo: upperZo
    
            }, () => updateArmRotation(armOutRotation, "left"), "leftArmOut", {
                leftUpperY: 0,
                leftUpperZ: rad(75),
                leftLowerZ: rad(85),
                leftUpperZo: upperZo
            });
        }
    }
    

}

/**
 * 更新上下手臂轉動角度
 * @param {*} rotation 手臂轉動角度
 * @param {*} side 左右邊
 */
function updateArmRotation(rotation, side) {
    if (side == "right") {
        const rightUpper = vrmManager.rotation(Bone.RightUpperArm);
        rightUpper.y = rotation.rightUpperY;
        rightUpper.z = rotation.rightUpperZ;
        vrmManager.rotation(Bone.RightLowerArm).z = rotation.rightLowerZ - rotation.rightUpperZo; //rightUpperZ
    } else if (side == "left") {
        const leftUpper = vrmManager.rotation(Bone.LeftUpperArm);
        leftUpper.y = rotation.leftUpperY;
        leftUpper.z = rotation.leftUpperZ;
        vrmManager.rotation(Bone.LeftLowerArm).z = rotation.leftLowerZ - rotation.leftUpperZo;
    }
}
       








        // if (mult == 1) { //right
        //     $.ajax({
        //         url: "http://localhost:57501/armOutRotate",
        //         data: JSON.stringify({
        //             shoulder: rightShoulder,
        //             elbow: rightElbow,
        //             wrist: rightWrist,
        //             midFin: rightMidFin,
        //             mult: mult,
        //             shoulderRotation: shoulderRotation

        //         }),
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         type: "POST",
        //         success: function(returnData) {
        //             upperY = returnData.upperY;
        //             upperZ = returnData.upperZ;
        //             lowerZ = returnData.lowerZ;
        //             upperZo = returnData.upperZo;

        //             rotateArmOut(upperY, upperZ, lowerZ, upperZo, mult);
        //         },
        //         error: function(xhr, ajaxOptions, thrownError) {
        //             console.log("error");
        //         }
        //     });

        // } else if (mult == -1) { //left
        //     $.ajax({
        //         url: "http://localhost:57501/armOutRotate",
        //         data: JSON.stringify({
        //             shoulder: leftShoulder,
        //             elbow: leftElbow,
        //             wrist: leftWrist,
        //             midFin: leftMidFin,
        //             mult: mult,
        //             shoulderRotation: shoulderRotation
        //         }),
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         type: "POST",
        //         success: function(returnData) {
        //             upperY = returnData.upperY;
        //             upperZ = returnData.upperZ;
        //             lowerZ = returnData.lowerZ;
        //             upperZo = returnData.upperZo;

        //             rotateArmOut(upperY, upperZ, lowerZ, upperZo, mult);
        //         },
        //         error: function(xhr, ajaxOptions, thrownError) {
        //             console.log("error");
        //         }
        //     });

        // }

// export function armOutCalc(app) {
//     // let flagUnDefined = 0;
//     app.post('/armOutRotate', (req, res) => {
        
//         const shoulder = req.body.shoulder;
//         const elbow = req.body.elbow;
//         const wrist = req.body.wrist;
//         const midFin = req.body.midFin;
//         const mult = req.body.mult;
//         const shoulderRotation = req.body.shoulderRotation;
        
//         let rot;

//         let armPart = [ shoulder, elbow, wrist, midFin ] ;

//         const [upperY,upperZ,lowerZ,upperZo] = moveArmOut(armPart, mult, shoulderRotation);
//         rot = [upperY,upperZ,lowerZ,upperZo];

//         res.send({
//             upperY: rot[0],
//             upperZ: rot[1],
//             lowerZ: rot[2],
//             upperZo: rot[3]
//         })
        
//     })
// }