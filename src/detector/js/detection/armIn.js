//用來存每一幀的旋轉角度
let armInRotation = {
    rightUpperY: 0,
    rightUpperZ: 0,
    rightLowerY: 0,
    rightLowerZ: 0,

    leftUpperY: 0,
    leftUpperZ: 0,
    leftLowerY: 0,
    leftLowerZ: 0,
};

// result為偵測點的位置，mult用來判斷左右邊
// mult等於一代表VRM的右邊、使用者的左邊、程式碼中寫left的部分，反之則為另一邊
function armInDetection(results, mult) {
    if (results.poseLandmarks != undefined) {
        //arm detect
        const leftShoulder = results.poseLandmarks[11];
        const leftElbow = results.poseLandmarks[13];
        //變數需先宣告，如果在下方if才宣告，若未跑進下方if時會出現錯誤(找不到變數)
        let leftWrist;
        let leftMidFin;
        let leftThumb;
        let leftLittleFin;
        if (results.leftHandLandmarks != undefined) {
            leftWrist = results.leftHandLandmarks[0];
            leftMidFin = results.leftHandLandmarks[9]
            leftThumb = results.leftHandLandmarks[2];
            leftLittleFin = results.leftHandLandmarks[17];
        }

        const rightShoulder = results.poseLandmarks[12];
        const rightElbow = results.poseLandmarks[14];
        let rightWrist; //在這邊定義 是為了因為下面有用到rightWrist 如果這裡rightHandLandmarks undefined的話 在裡面宣告的話 會沒進去 沒宣告到  ->下面要用到rightWrist的時候會錯
        let rightMidFin; 
        let rightThumb;
        let rightLittleFin;
        if (results.rightHandLandmarks != undefined) {
            rightWrist = results.rightHandLandmarks[0];
            rightMidFin = results.rightHandLandmarks[9];
            rightThumb = results.rightHandLandmarks[2];
            rightLittleFin = results.rightHandLandmarks[17];
        }

        let armPart = [
            [rightShoulder, rightElbow, rightWrist, rightMidFin, rightThumb, rightLittleFin],
            [leftShoulder, leftElbow, leftWrist, leftMidFin, leftThumb, leftLittleFin]
        ]

        if (armPart[0][0] && armPart[1][0]) { //if (armPart[0][0] && armPart[0][1]) {
            if (mult == 1) {
                moveArmIn(armPart[0], mult)
            } else if (mult == -1) {
                moveArmIn(armPart[1], mult)
            }
        }
    }
}

/**
 * 計算手臂旋轉角度
 * @param {*} arm 該手臂的值 0:肩膀 1:手肘 2:手腕 3:中指 4:大拇指 5:小指 
 * @param {*} mult 加權值  1:vrm右手 -1:vrm左手
 */
function moveArmIn(arm, mult) {

    if (arm[0].visibility >= 0.65 && arm[1].visibility >= 0.65 && arm[2]) { //當visibility >= 0.65 時在webcam的畫面上才會出現偵測點及線(有偵測到)

        // XZ平面上，求上手臂Y的轉動角度
        // getAngle 需要以Vector3形式才可計算
        // pointToVec = new THREE.Vector3 -> 都是將偵測點的資料轉為Vector3的形式，差別為pointToVec只須給部位就會將該部位的XYZ變成Vector3的形式，反之則需要給三個值才可轉成Vector3的形式
        let shoulderVecY = pointToVec(arm[0]); //肩膀的XYZ
        let elbowVecY = new THREE.Vector3(arm[1].x, arm[0].y, arm[1].z); //elbow的x，shoulder的y，elbow的z
        let upperExtendY = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z); //shoulder的x左右延伸，shoulder的yz
        // getAngle為利用三點求角度的function
        let upperY = (getAngle(elbowVecY, shoulderVecY, upperExtendY) - Math.PI / 8) * mult; //上手臂y轉動的角度

        // XY平面上，求上手臂Z的轉動角度
        let shoulderVecZ = pointToVec(arm[0]) //肩膀點
        let elbowVecZ = new THREE.Vector3(arm[1].x, arm[1].y, arm[0].z) //elbow的xy，shoulder的z
        let upperExtendZ = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z) //shoulder的x左右延伸、shoulder的yz
        let upperZ = getAngle(elbowVecZ, shoulderVecZ, upperExtendZ) * mult; //上手臂z轉動的角度

        if (arm[0].y < arm[1].y) { //手肘低於肩膀
            upperZ *= -1
        }
        
        if (arm[1].z > arm[0].z) { //手肘後於肩膀
            upperY *= (-1.2);
        }

        //手進肚子 (upperY乘-0.3為了讓手臂敞開一點)
        if(-(arm[0].x + 0.3 > arm[2].x) * mult && arm[1].y < arm[2].y){
            upperY *= -0.3 * mult
        }
        //更新armInRotation中的值
        if (mult == 1) { //vrm的右手動畫
            vrmManager.tween(armInRotation, {
                rightUpperY: upperY,
                rightUpperZ: upperZ
            }, () => updateUpperArmRotation(armInRotation, "right"), "rightUpperArm", {
                rightUpperY: 0,
                rightUpperZ: rad(-75)
            });
        } else if (mult == -1) { //vrm的左手動畫
            vrmManager.tween(armInRotation, {
                leftUpperY: upperY,
                leftUpperZ: upperZ
            }, () => updateUpperArmRotation(armInRotation, "left"), "leftUpperArm", {
                leftUpperY: 0,
                leftUpperZ: rad(75)
            });
        }

        // XZ平面上，求下手臂Y的轉動角度
        let lowerElbowVecY = pointToVec(arm[1]); //手肘點
        let wristVecY = new THREE.Vector3(arm[2].x, arm[1].y, arm[2].z); //手腕的x，手肘的y，手腕的z
        let lowerExtendY = new THREE.Vector3(arm[1].x - (10 * mult), arm[1].y, arm[1].z); //elbow的x左右延伸，elbow的yz
        let lowerY = getAngle(wristVecY, lowerElbowVecY, lowerExtendY) * mult; //下手臂的y轉動角度

        //x y
        let wristVecZ = new THREE.Vector3(arm[2].x, arm[2].y, arm[1].z); //手腕的xy，手肘的z
        let lowerExtendZ = new THREE.Vector3(arm[1].x - (10 * mult), arm[1].y, arm[1].z); //elbow的x左右延伸，elbow的yz
        let lowerZ = getAngle(wristVecZ, lowerElbowVecY, lowerExtendZ) * mult; //下手臂的z轉動角度

        lowerZ -= Math.PI * mult;

        if ((arm[2].y < arm[1].y) * mult) {// 手腕高於手肘
            lowerZ *= -1;
        }
        //更新armInRotation中的值
        if (mult == 1) { //vrm下右手臂動畫
            vrmManager.tween(armInRotation, {
                rightLowerZ: lowerZ,
                rightLowerY: lowerY
            }, () => updateLowerArmRotation(armInRotation, "right"), "rightLowerArm", {
                rightLowerZ: rad(-85),
                rightLowerY: 0
            });
        } else if (mult == -1) { //vrm下左手臂動畫
            vrmManager.tween(armInRotation, {
                leftLowerZ: lowerZ,
                leftLowerY: lowerY
            }, () => updateLowerArmRotation(armInRotation, "left"), "leftLowerArm", {
                leftLowerZ: rad(85),
                leftLowerY: 0
            });
        }

    } else {
        //若沒偵測到手
        if (mult == 1) {            //VRM右手初始動作(手平舉姿勢)
            vrmManager.rotation(Bone.RightUpperArm).y = 0;
            vrmManager.rotation(Bone.RightUpperArm).z = rad(-75);
            vrmManager.rotation(Bone.RightLowerArm).y = 0;
            vrmManager.rotation(Bone.RightLowerArm).z = 0;
            vrmManager.rotation(Bone.RightHand).z = 0;
        } else if (mult == -1) {    //VRM左手初始動作(手平舉姿勢)
            vrmManager.rotation(Bone.LeftUpperArm).y = 0;
            vrmManager.rotation(Bone.LeftUpperArm).z = rad(75);
            vrmManager.rotation(Bone.LeftLowerArm).y = 0;
            vrmManager.rotation(Bone.LeftLowerArm).z = 0;
            vrmManager.rotation(Bone.LeftHand).z = 0;
        }
    }
}


/**
 * 更新vrm上手臂的轉動角度【手在身體內】
 * @param {*} rotation 轉動角度
 * @param {*} side 左右邊
 */
function updateUpperArmRotation(rotation, side) {
    if (side == "right") {
        const rightUpper = vrmManager.rotation(Bone.RightUpperArm);
        rightUpper.y = rotation.rightUpperY;
        rightUpper.z = rotation.rightUpperZ;
    } else if (side == "left") {
        const leftUpper = vrmManager.rotation(Bone.LeftUpperArm);
        leftUpper.y = rotation.leftUpperY;
        leftUpper.z = rotation.leftUpperZ;
    }
}

/**
 * 更新vrm下手臂的轉動角度【手在身體內】
 * @param {*} rotation 轉動角度
 * @param {*} side 左右邊
 */
function updateLowerArmRotation(rotation, side) {
    if (side == "right") {
        vrmManager.rotation(Bone.RightLowerArm).z = rotation.rightLowerZ;
        vrmManager.rotation(Bone.RightLowerArm).y = rotation.rightLowerY;
    } else if (side == "left") {
        vrmManager.rotation(Bone.LeftLowerArm).z = rotation.leftLowerZ;
        vrmManager.rotation(Bone.LeftLowerArm).y = rotation.leftLowerY;
    }
}



// 後端
// let armInRotation = {
//     rightUpperY: 0,
//     rightUpperZ: 0,
//     rightLowerY: 0,
//     rightLowerZ: 0,

//     leftUpperY: 0,
//     leftUpperZ: 0,
//     leftLowerY: 0,
//     leftLowerZ: 0,
// };

// let upperY;
// let upperZ;
// let lowerY;
// let lowerZ;

// function armInDetection(results, mult) {

//     if (results.poseLandmarks != undefined) {
//         //arm detect
//         const leftShoulder = results.poseLandmarks[11];
//         const leftElbow = results.poseLandmarks[13];
//         let leftWrist;
//         if (results.leftHandLandmarks != undefined) {
//             leftWrist = results.leftHandLandmarks[0];
//         }

//         const rightShoulder = results.poseLandmarks[12];
//         const rightElbow = results.poseLandmarks[14];
//         let rightWrist;
//         if (results.rightHandLandmarks != undefined) {
//             rightWrist = results.rightHandLandmarks[0]; //這邊程式碼wrist的部分是連過去handpose
//         }


//         if (mult == 1) {
//             // $.ajax({
//             //     url: "http://localhost:57501/armInRotate",
//             //     data: JSON.stringify({
//             //         mult: mult,
//             //         shoulder: rightShoulder,
//             //         elbow: rightElbow,
//             //         wrist: rightWrist
//             //     }),
//             //     dataType: "json",
//             //     contentType: "application/json; charset=utf-8",
//             //     type: "POST",
//             //     success: function(returnData) {
//             //         upperY = returnData.upperY;
//             //         upperZ = returnData.upperZ;
//             //         lowerY = returnData.lowerY;
//             //         lowerZ = returnData.lowerZ;
//             //         rotateArmIn(upperY, upperZ, lowerY, lowerZ, mult);
//             //     },
//             //     error: function(xhr, ajaxOptions, thrownError) {
//             //         console.log("error");
//             //     }
//             // });

//             let upperY,upperZ,lowerY,lowerZ = armInCalc(mult,rightShoulder,rightElbow,rightWrist);
//             rotateArmIn(upperY, upperZ, lowerY, lowerZ, mult);

//         } else if (mult == -1) {
//             // $.ajax({
//             //     url: "http://localhost:57501/armInRotate",
//             //     data: JSON.stringify({
//             //         mult: mult,
//             //         shoulder: leftShoulder,
//             //         elbow: leftElbow,
//             //         wrist: leftWrist
//             //     }),
//             //     dataType: "json",
//             //     contentType: "application/json; charset=utf-8",
//             //     type: "POST",
//             //     success: function(returnData) {
//             //         upperY = returnData.upperY;
//             //         upperZ = returnData.upperZ;
//             //         lowerY = returnData.lowerY;
//             //         lowerZ = returnData.lowerZ;
//             //         rotateArmIn(upperY, upperZ, lowerY, lowerZ, mult);
//             //     },
//             //     error: function(xhr, ajaxOptions, thrownError) {
//             //         console.log("error");
//             //     }
//             // });
//             let upperY,upperZ,lowerY,lowerZ = armInCalc(mult,leftShoulder,leftElbow,leftWrist);
//             rotateArmIn(upperY, upperZ, lowerY, lowerZ, mult);
//         }
//     }
//     else{
//         rotateArmIn(0,0,0,0,mult)
//     }
// }



// function rotateArmIn(upperY, upperZ, lowerY, lowerZ, mult) {
//     if(upperY == 0 && upperZ == 0 && lowerY == 0 && lowerZ == 0){
//         console.log("front")
//         if (mult == 1) { //vrm的右手動畫
//             vrmManager.rotation(Bone.RightUpperArm).y = 0;
//             vrmManager.rotation(Bone.RightUpperArm).z = rad(-75);
//             vrmManager.rotation(Bone.RightLowerArm).y = 0;
//             vrmManager.rotation(Bone.RightLowerArm).z = 0;
//         } else if (mult == -1) { //vrm的左手動畫
//             vrmManager.rotation(Bone.LeftUpperArm).y = 0;
//             vrmManager.rotation(Bone.LeftUpperArm).z = rad(75);
//             vrmManager.rotation(Bone.LeftLowerArm).y = 0;
//             vrmManager.rotation(Bone.LeftLowerArm).z = 0;
//         }
//     }
//     else{
//         if (mult == 1) { //vrm的右手動畫
//             vrmManager.tween(armInRotation, {
//                 rightUpperY: upperY,
//                 rightUpperZ: upperZ,
//                 rightLowerZ: lowerZ,
//                 rightLowerY: lowerY
//             }, () => updateUpperArmRotation(armInRotation, "right"), "rightArmIn", {
//                 rightUpperY: 0,
//                 rightUpperZ: rad(-75),
//                 rightLowerZ: rad(-85),
//                 rightLowerY: 0
//             });
    
//         } else if (mult == -1) { //vrm的左手動畫
//             vrmManager.tween(armInRotation, {
//                 leftUpperY: upperY,
//                 leftUpperZ: upperZ,
//                 leftLowerZ: lowerZ,
//                 leftLowerY: lowerY
//             }, () => updateUpperArmRotation(armInRotation, "left"), "leftArmIn", {
//                 leftUpperY: 0,
//                 leftUpperZ: rad(75),
//                 leftLowerZ: rad(85),
//                 leftLowerY: 0
//             });
//         }    
//     }  
// }


// /**
//  * 更新vrm手臂的轉動角度【手在身體內】
//  * @param {*} rotation 轉動角度
//  * @param {*} side 左右邊
//  */
// function updateUpperArmRotation(rotation, side) {
//     if (side == "right") {
//         const rightUpper = vrmManager.rotation(Bone.RightUpperArm);
//         rightUpper.y = rotation.rightUpperY;
//         rightUpper.z = rotation.rightUpperZ;
//         vrmManager.rotation(Bone.RightLowerArm).z = rotation.rightLowerZ;
//         vrmManager.rotation(Bone.RightLowerArm).y = rotation.rightLowerY;
//     } else if (side == "left") {
//         const leftUpper = vrmManager.rotation(Bone.LeftUpperArm);
//         leftUpper.y = rotation.leftUpperY;
//         leftUpper.z = rotation.leftUpperZ;
//         vrmManager.rotation(Bone.LeftLowerArm).z = rotation.leftLowerZ;
//         vrmManager.rotation(Bone.LeftLowerArm).y = rotation.leftLowerY;
//     }
// }


// function armInCalc(mult,shoulder,elbow,wrist){
//     // let flagUnD = 0;
    
//     // const mult = req.body.mult;
//     // const shoulder = req.body.shoulder;
//     // const elbow = req.body.elbow;
//     // const wrist = req.body.wrist;

//     let armPart = [shoulder,elbow,wrist]
//     // let rot;

//     let upperY,upperZ,lowerY,lowerZ = moveArmIn(armPart, mult)
//     // rot = [upperY,upperZ,lowerY,lowerZ];

//     // res.send({
//     //     upperY: rot[0],
//     //     upperZ: rot[1],
//     //     lowerY: rot[2],
//     //     lowerZ: rot[3]
//     // })
//     return upperY,upperZ,lowerY,lowerZ;
    
// }

// /**
//  * 手在身體裡面的arm動作
//  * @param {*} arm 該手臂的值 0:肩膀 1:手肘 2:手腕 3:中指 4:大拇指 5:小指 
//  * @param {*} mult 加權值  1:vrm右手 -1:vrm左手
//  */
//  function moveArmIn(arm, mult) {
//     if (arm[0].visibility >= 0.65 && arm[1].visibility >= 0.65 && arm[2]) { //right upper

//         //x z
//         let shoulderVecY = pointToVec(arm[0]); //肩膀點

//         let elbowVecY = new THREE.Vector3(arm[1].x, arm[0].y, arm[1].z); //shoulder的y，elbow的xz
//         let upperExtendY = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z); //shoulder的yz,shoulder的x左右延伸
//         let upperY = (getAngle(elbowVecY, shoulderVecY, upperExtendY) - Math.PI / 8) * mult; //上手臂y轉動的角度

//         //x y
//         let shoulderVecZ = pointToVec(arm[0]) //肩膀點
//         let elbowVecZ = new THREE.Vector3(arm[1].x, arm[1].y, arm[0].z) //shoulder的z，elbow的xy
//         let upperExtendZ = new THREE.Vector3(arm[0].x - (10 * mult), arm[0].y, arm[0].z) //shoulder的yz,shoulder的x左右延伸
//         let upperZ = getAngle(elbowVecZ, shoulderVecZ, upperExtendZ) * mult; //上手臂z轉動的角度


//         if (arm[0].y < arm[1].y) { //手肘低於肩膀
//             upperZ *= -1
//         }
//         if (arm[0].z < arm[1].z) { //手肘後於肩膀
//             upperY *= (-1.3) * mult;
//         }
        
//         // x z
//         let lowerElbowVecY = pointToVec(arm[1]); //手肘點
//         let wristVecY = new THREE.Vector3(arm[2].x, arm[1].y, arm[2].z); //手腕的xz，手肘的y
//         let lowerExtendY = new THREE.Vector3(arm[1].x - (10 * mult), arm[1].y, arm[1].z); //elbow的yz,elbow的x左右延伸
//         let lowerY = getAngle(wristVecY, lowerElbowVecY, lowerExtendY) * mult; //下手臂的y轉動角度

//         //x y
//         let wristVecZ = new THREE.Vector3(arm[2].x, arm[2].y, arm[1].z); //手腕的xy，手肘的z
//         let lowerExtendZ = new THREE.Vector3(arm[1].x - (10 * mult), arm[1].y, arm[1].z); //elbow的yz,elbow的x左右延伸
//         let lowerZ = getAngle(wristVecZ, lowerElbowVecY, lowerExtendZ) * mult; //下手臂的z轉動角度

//         lowerZ -= Math.PI * mult;

//         if (arm[1].y < arm[2].y){
//             upperY = -0.6 * mult
//         }

//         if ((arm[2].y < arm[1].y) * mult) {
//             lowerZ *= -1;
//         }
        
//         return[upperY, upperZ, lowerY, lowerZ];
//     }
//     else {
//         //若沒偵測到手
//         // flagUnD = 1;
//         return[0,0,0,0];
//     }
//  }