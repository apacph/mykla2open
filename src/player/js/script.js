let scene = new THREE.Scene();

let camera = new THREE.PerspectiveCamera(10, document.getElementById("KLA").clientWidth / document.getElementById("KLA").clientHeight, 0.1, 20);
// camera.position.set(0, 0.5, 11.4);
camera.position.x = 0;
camera.position.y = 0.5;
camera.position.z = 11.4;

let renderer = new THREE.WebGLRenderer({
    antialias: true,
    alpha: true
});



renderer.setSize(document.getElementById("KLA").clientWidth, document.getElementById("KLA").clientHeight);
// renderer.setClearColor(0xe3ebf1, 1.0);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.domElement.setAttribute("id", "vrmWindow");
document.getElementById("KLA").appendChild(renderer.domElement);

window.addEventListener('resize', function() {
    let width = document.getElementById("KLA").clientWidth;
    let height = document.getElementById("KLA").clientHeight;
    renderer.setSize(width, height);
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
});

//眼球看的方向
let lookAtTarget = new THREE.Object3D();
camera.add(lookAtTarget);

// light
let light = new THREE.DirectionalLight(0xffffff);
light.position.set(1.0, 1.0, 1.0).normalize();
scene.add(light);

let load1 = 0;
let allVrms = [];
let vrmManager;
let adjustHipPosition = 0;

vrmLoad("GirlLongHair");

function vrmLoad(vrmName) {
    document.getElementById("loading").style.display = "flex";
    document.getElementById("loading").style.opacity = 1;
    document.getElementById("noticeKLA").innerHTML = "Wait for loading character...";
    let loader = new THREE.GLTFLoader();
    loader.crossOrigin = 'anonymous';
    loader.load(

        // URL of the VRM you want to load
        './vrm/' + vrmName + '.vrm',


        // called when the resource is loaded
        (gltf) => {


            THREE.VRMUtils.removeUnnecessaryJoints(gltf.scene);

            // generate a VRM instance from gltf
            THREE.VRM.from(gltf).then((vrm) => {

                vrmManager = new VRMManager(vrm);
                // add the loaded vrm to the scene
                scene.add(vrm.scene);

                // console.log(vrm);
                allVrms[0] = vrm;


                // deal with vrm features
                // turn
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.Hips).rotation.y = Math.PI;
                adjustHipPosition = 1 - vrm.humanoid.humanBones.hips[0].node.position.y;
                vrm.humanoid.humanBones.hips[0].node.position.y = 1;

                // vrm load完 手擺放的位置
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.LeftUpperArm).rotation.z = Math.PI / 2 - 0.35;
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.RightUpperArm).rotation.z = -(Math.PI / 2 - 0.35);
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.LeftHand).rotation.z = 0.1;
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.RightHand).rotation.z = -0.1;
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.LeftLowerArm).rotation.z = 0.15;
                vrm.humanoid.getBoneNode(THREE.VRMSchema.HumanoidBoneName.RightLowerArm).rotation.z = -0.15;

                vrm.lookAt.target = lookAtTarget;

                load1 = 1;
                if (load1) {
                    document.getElementById("noticeKLA").innerHTML = "Done!";
                    setTimeout("block_noticeKLA()", 3000);
                }

            });

        },

        // called while loading is progressing
        (progress) => console.log('Loading model...', 100.0 * (progress.loaded / progress.total), '%'),

        // called when loading has errors
        (error) => console.error(error)

    );
}


const gridHelper = new THREE.GridHelper(10, 10);
gridHelper.position.set(0, 0.098, 0);
scene.add(gridHelper);

let orbitControls = new THREE.OrbitControls(camera, renderer.domElement); //讓vrm可以在3D空間中轉動
orbitControls.screenSpacePanning = true;
orbitControls.target.set(0, 1, 0);
orbitControls.update();



let clock = new THREE.Clock();

// const color = 'aqua';
// const lineWidth = 2;

// let switchAnimation = 0;
let frame = 0;

// let AD = 1;
let playStatus = 0;

function calculateTime(time, min, sec) {
    if (Math.floor(time) >= 60) { //到分鐘
        if ((Math.floor(time)) / 60 < 10) {
            document.getElementById(min).innerHTML = "0" + Math.floor(time / 60);
            if (Math.floor(time) % 60 < 10) {
                document.getElementById(sec).innerHTML = "0" + (time % 60).toFixed(2);
            } else {
                document.getElementById(sec).innerHTML = (time % 60).toFixed(2);
            }
        } else {
            document.getElementById(min).innerHTML = Math.floor(time / 60);
            if (Math.floor(time) % 60 < 10) {
                document.getElementById(sec).innerHTML = "0" + (time % 60).toFixed(2);
            } else {
                document.getElementById(sec).innerHTML = (time % 60).toFixed(2);
            }
        }
    } else { //只有到秒
        if (Math.floor(time) < 10) {
            document.getElementById(sec).innerHTML = "0" + time;
        } else {
            document.getElementById(sec).innerHTML = time;
        }
    }
}


animateDefault();

function animateDefault() {
    if (playStatus == 0) {
        if (allVrms[0]) { //假如vrm已經載在畫面了 在animateDefaultu也要讓vrm本身更新動作 
            const deltaTime = clock.getDelta();
            allVrms[0].update(deltaTime);

        }
        requestAnimationFrame(animateDefault);
        renderer.render(scene, camera); //渲染在畫面上
    }

}

let specialFlag = false; //每執行一個frame會變成false，執行過special會變成true => 那個frame如果有special ，且已經開始進行過pause三秒(flag=true)的話 就不要再開始一次
let specialTimeout;

function animate() {

    if (playStatus == 0) { //按下pause stop時 不會再跑最後一次animate了 (剛剛execute時的最後遞迴)
        return;
    }

    document.getElementById("pause").disabled = false;

    //判斷分秒
    let nowTime = clock.elapsedTime;
    if (nowTime >= animation[animation.length - 1].time.Time.toFixed(2)) {
        nowTime = animation[animation.length - 1].time.Time;
    }
    calculateTime(nowTime.toFixed(2), "minute", "second");

    const deltaTime = clock.getDelta();


    if (playStatus == 1) { //execute情況

        if (frame < animation.length) {
            const frameTime = animation[frame].time.Time;
            if (nowTime >= frameTime) {
                everyNode(frame);
                specialFlag = false; //代表此幀還未執行過暫停三秒 
                frame += 1;
            }
            if (frame != 0) {
                // console.log(nowTime.toFixed(2) + " " + animation[frame - 1].editorTime.toFixed(2));
                //沒書籤按紐了 現在都一定要在特殊標記地方暫停三秒
                if (specialFrame.includes(frame - 1) && specialFlag == false) {
                    const specialTime = animation[frame - 1].editorTime;
                    countDown();
                    pauseVRM(specialTime); //暫停
                    document.getElementById("pause").disabled = true; //使用者不得在執行暫停三秒時按暫停按鈕
                    // document.getElementById("forward").disabled = true; //使用者不得在執行暫停三秒時按forward按鈕                
                    // document.getElementById("backward").disabled = true; //使用者不得在執行暫停三秒時按backward按鈕  
                    specialFlag = true;
                    specialTimeout = setTimeout(() => {
                        document.getElementById("pause").disabled = false; //三秒後，使用者就可以按暫停按鈕
                        // document.getElementById("forward").disabled = false; //三秒後，使用者就可以按forward按鈕
                        // document.getElementById("backward").disabled = false; //三秒後，使用者就可以按backward按鈕
                        if (!undetected) { //偵測中
                            execute(); //三秒後 經由executeVRM()進入animate() 虛擬人物繼續執行動作
                        } //沒偵測的話 vrm動作就繼續停著 需要自己觸發播放鍵
                    }, 3000);


                }

            }

        }


        if (frame >= animation.length) {
            let loopBtn = document.getElementById("loopBtn");
            if (loopBtn.checked) {
                frame = 0;
                document.getElementById("second").innerHTML = "00.00";
                document.getElementById("minute").innerHTML = "00";
                clock.elapsedTime = 0;
                if (voiceData.length != 0) {
                    stopSound();
                    playSound();
                }
            } else if (!loopBtn.checked) {
                end();
            }

        }
    }
    requestAnimationFrame(animate);
    TWEEN.update(); //TWEEN補間動畫更新
    allVrms[0].update(deltaTime)
    renderer.render(scene, camera); //!
}


function positionReset() { //動作重置function
    vrmManager.setPreset(Preset.BlinkR, 0);
    vrmManager.setPreset(Preset.BlinkL, 0);
    vrmManager.setLookAtTarget(0, 0);

    allVrms[0].blendShapeProxy._blendShapeGroups.A.weight = 0;
    allVrms[0].blendShapeProxy._blendShapeGroups.E.weight = 0;
    allVrms[0].blendShapeProxy._blendShapeGroups.I.weight = 0.2;
    allVrms[0].blendShapeProxy._blendShapeGroups.O.weight = 0;

    vrmManager.rotation(Bone.Neck).x = 0;
    vrmManager.rotation(Bone.Neck).y = 0;
    vrmManager.rotation(Bone.Neck).z = 0;

    vrmManager.rotation(Bone.Spine).x = 0;
    vrmManager.rotation(Bone.Spine).y = 0;
    vrmManager.rotation(Bone.Spine).z = 0;

    vrmManager.rotation(Bone.RightUpperArm).y = 0;
    vrmManager.rotation(Bone.RightUpperArm).z = -(Math.PI / 2 - 0.35);
    vrmManager.rotation(Bone.RightLowerArm).y = 0;
    vrmManager.rotation(Bone.RightLowerArm).z = -0.15;
    vrmManager.rotation(Bone.LeftUpperArm).y = 0;
    vrmManager.rotation(Bone.LeftUpperArm).z = Math.PI / 2 - 0.35;
    vrmManager.rotation(Bone.LeftLowerArm).y = 0;
    vrmManager.rotation(Bone.LeftLowerArm).z = 0.15;

    vrmManager.rotation(Bone.RightHand).z = -0.1;
    vrmManager.rotation(Bone.LeftHand).z = 0.1;
    vrmManager.rotation(Bone.RightThumbProximal).y = 0;
    vrmManager.rotation(Bone.RightThumbIntermediate).y = 0;
    vrmManager.rotation(Bone.RightThumbDistal).y = 0;
    vrmManager.rotation(Bone.RightIndexProximal).x = 0;
    vrmManager.rotation(Bone.RightIndexProximal).z = 0;
    vrmManager.rotation(Bone.RightIndexIntermediate).z = 0;
    vrmManager.rotation(Bone.RightIndexDistal).z = 0;
    vrmManager.rotation(Bone.RightMiddleProximal).z = 0;
    vrmManager.rotation(Bone.RightMiddleIntermediate).z = 0;
    vrmManager.rotation(Bone.RightMiddleDistal).z = 0;
    vrmManager.rotation(Bone.RightRingProximal).x = 0;
    vrmManager.rotation(Bone.RightRingProximal).z = 0;
    vrmManager.rotation(Bone.RightRingIntermediate).z = 0;
    vrmManager.rotation(Bone.RightRingDistal).z = 0;
    vrmManager.rotation(Bone.RightLittleProximal).x = 0;
    vrmManager.rotation(Bone.RightLittleProximal).z = 0;
    vrmManager.rotation(Bone.RightLittleIntermediate).z = 0;
    vrmManager.rotation(Bone.RightLittleDistal).z = 0;
    vrmManager.rotation(Bone.LeftThumbProximal).y = 0;
    vrmManager.rotation(Bone.LeftThumbIntermediate).y = 0;
    vrmManager.rotation(Bone.LeftThumbDistal).y = 0;
    vrmManager.rotation(Bone.LeftIndexProximal).x = 0;
    vrmManager.rotation(Bone.LeftIndexProximal).z = 0;
    vrmManager.rotation(Bone.LeftIndexIntermediate).z = 0;
    vrmManager.rotation(Bone.LeftIndexDistal).z = 0;
    vrmManager.rotation(Bone.LeftMiddleProximal).z = 0;
    vrmManager.rotation(Bone.LeftMiddleIntermediate).z = 0;
    vrmManager.rotation(Bone.LeftMiddleDistal).z = 0;
    vrmManager.rotation(Bone.LeftRingProximal).x = 0;
    vrmManager.rotation(Bone.LeftRingProximal).z = 0;
    vrmManager.rotation(Bone.LeftRingIntermediate).z = 0;
    vrmManager.rotation(Bone.LeftRingDistal).z = 0;
    vrmManager.rotation(Bone.LeftLittleProximal).x = 0;
    vrmManager.rotation(Bone.LeftLittleProximal).z = 0;
    vrmManager.rotation(Bone.LeftLittleIntermediate).z = 0;
    vrmManager.rotation(Bone.LeftLittleDistal).z = 0;
    vrmManager.rotation(Bone.RightUpperLeg).x = 0;
    vrmManager.rotation(Bone.RightUpperLeg).z = 0;
    vrmManager.rotation(Bone.RightLowerLeg).x = 0;
    vrmManager.rotation(Bone.LeftUpperLeg).x = 0;
    vrmManager.rotation(Bone.LeftUpperLeg).z = 0;
    vrmManager.rotation(Bone.LeftLowerLeg).x = 0;


    // 還原到預設位置(需還原才能讓下方左右foot與toes方便與地面比較)
    allVrms[0].humanoid.humanBones.hips[0].node.position.y = 1;
    console.log("positionReset" + allVrms[0].humanoid.humanBones.hips[0].node.position.y)
}




let newBeginStatus = 1;
let endStatus = 0;
let stopTime;


function clickExecute() {
    document.getElementById("start").disabled = true; //不讓使用者在偵測距離期間 一直不斷按下播放按紐 會壞掉(?)
    document.getElementById("pause").disabled = true; //讓偵測使用者距離期間還不能按pause 等開始跑vrm 進到animate()時 才能按pause
    if (playStatus == 1) {
        // 若已經在執行，則不再處理
        return;
    }
    if (animation.length == 0) { //若沒有選擇檔案
        disappearAlertPanel();
        const alertContent = document.getElementById("alertContent");
        alertContent.style.visibility = "visible";
        const confirmBtn = document.getElementById("confirmBtn");
        confirmBtn.style.display = "inline-block";
        confirmBtn.onclick = () => { disappearAlertPanel() };
        alertContent.innerHTML = "please choose the right json file first !";
        return;
    }
    endStatus = 0; //mediapipeDetector.js那就需要用到這些判斷撥放等等狀態的flag了 所以一點下撥放按鈕 就需要改變flag
    playStatus = 1;
    stopWarning();
    userZFlag = 3;
}
//沒有錄音的execute
function execute() {
    endStatus = 0; //若從specialFrame暫停三秒後會進execute() 這邊也需要改一下狀態flag情況 因為畢竟剛剛暫停三秒期間 playStatus = 0
    playStatus = 1;
    stopWarning();

    if (newBeginStatus == 1) {
        if (voiceData.length != 0) {
            playSound();
        }

        frame = 0;
        clock.elapsedTime = 0;
        stopTime = 0;
        // positionReset();
        clock.start();
        newBeginStatus = 0; // 開始後將 newBeginStatus 改為 0

        startVideoRecord();
    } else if (newBeginStatus == 0) { // 若非從頭開始，從上次暫停的時間開始
        if (voiceData.length != 0) {
            resumeSound();
        }
        clock.start();
        clock.elapsedTime = stopTime;
        // 將暫停時間重設為0
        stopTime = 0;

        resumeVideoRecord()
    }

    animate();

}

//沒有暫停錄音的pause
function pauseVRM(specialTime = null) {

    if (animation.length == 0) {
        return;
    }
    if (specialTime != null) {
        stopTime = specialTime; //執行special暫停時的時間
    } else {
        stopTime = clock.elapsedTime; //直接按暫停的時間
    }
    clock.stop();
    // 紀錄暫停時間
    calculateTime(stopTime.toFixed(2), "minute", "second");

    newBeginStatus = 0;
    playStatus = 0;
    endStatus = 0;
    animateDefault();
}

function pause() {
    document.getElementById("start").disabled = false;
    userZFlag = 0;
    flaggg = 0; //flaggg預設為0 所以按pause之後 要先重設

    if (voiceData.length != 0) {
        pauseSound();
    }

    // if (animation.length == 0) {
    //     return;
    // }

    stopTime = clock.elapsedTime; //直接按暫停的時間
    clock.stop();
    // 紀錄暫停時間

    calculateTime(stopTime.toFixed(2), "minute", "second");

    newBeginStatus = 0;
    playStatus = 0;
    endStatus = 0;
    animateDefault();
    pauseVideoRecord();
}

function stop() {
    if (recordTimer != undefined) {
        clearInterval(recordTimer); //停止倒數
        defaultAnimation();
    }

    defaultLoading();


    document.getElementById("start").disabled = false;
    document.getElementById("pause").disabled = true;

    userZFlag = 0;
    flaggg = 0; //flaggg預設為0 所以按stop之後 要先重設

    if (voiceData.length != 0) {
        stopSound();
    }
    // const state = mediaRecorder.state;
    // if (btnVoice.checked && state != "inactive") {
    //     voiceStop();
    // }
    clock.stop();

    // document.getElementById("forward").disabled = false; //stop後，使用者就可以按forward按鈕
    // document.getElementById("backward").disabled = false; //stop後，使用者就可以按backward按鈕

    if (specialTimeout != undefined) {
        clearTimeout(specialTimeout); //如果setTimeout還未被執行，可以使用clearTimeout()來阻止它
        stopCountDown(); //停止倒數
        defaultAnimation();
    }
    // specialPartList = [];
    // allScoreList = [];
    // editTimeList = [];

    document.getElementById("num").innerHTML = ""; //321的字在按stop時清空掉
    allScoreList = [];
    for (let seqArr = 0; seqArr < sequence.length; seqArr++) { //清空所有動作名稱的editJson的arr -> (每個不同的動作名稱arr用來存放用來顯示在表格的各個時間 部位 分數)
        let actionName = sequence[seqArr];
        editJson[actionName].arr = [];
        // let a = "歪頭"
        // console.log(editJson[a].arr);

    }

    positionReset();

    frame = 0;
    clock.elapsedTime = 0;
    stopTime = 0;

    document.getElementById("second").innerHTML = "00.00";
    document.getElementById("minute").innerHTML = "00";

    newBeginStatus = 1;
    playStatus = 0;
    endStatus = 0;
    animateDefault();
    resetTweenData();
    // btnVoice.disabled = false;

    stopVideoRecord();
}

function end() {
    document.getElementById("start").disabled = false;
    clock.stop();
    stopSound();

    // if (specialTimeout != undefined) { //現在結束的時候 不會做編輯了
    //     clearTimeout(specialTimeout);
    // }

    // console.log(clock.elapsedTime)
    // frame = 0;
    // clock.elapsedTime = 0;
    // stopTime = 0;

    newBeginStatus = 1;
    playStatus = 0;
    endStatus = 1;
    document.getElementById("pause").disabled = true;
    animateDefault();
    resetTweenData();

    //播完後 讓scoreList顯示出來
    document.getElementById("scoreList").style.display = "block";
    countTimes(); //最後評分標語次數

    // if (btnVoice.checked) {
    //     voiceStop();
    // }
    stopVideoRecord();
}


//-------------------------everyNode (includes tween)--------------------------

let hipsPosition = {
    y: 0
};

function updateHipsPosition(position) {
    allVrms[0].humanoid.humanBones.hips[0].node.position.y = position.y + adjustHipPosition;
}

let rightUpperLegRotation = {
    x: 0,
    z: 0
};

function updateRightUpperLegRotation(rotation) {
    const rightUpperLegNode = vrmManager.rotation(Bone.RightUpperLeg);
    rightUpperLegNode.x = rotation.x;
    rightUpperLegNode.z = rotation.z;
}

let rightLowerLegRotation = {
    x: 0
};

function updateRightLowerLegRotation(rotation) {
    const rightLowerLegNode = vrmManager.rotation(Bone.RightLowerLeg);
    rightLowerLegNode.x = rotation.x;
}

let leftUpperLegRotation = {
    x: 0,
    z: 0
};

function updateLeftUpperLegRotation(rotation) {
    const leftUpperLegNode = vrmManager.rotation(Bone.LeftUpperLeg);
    leftUpperLegNode.x = rotation.x;
    leftUpperLegNode.z = rotation.z;
}

let leftLowerLegRotation = {
    x: 0
};

function updateLeftLowerLegRotation(rotation) {
    const leftLowerLegNode = vrmManager.rotation(Bone.LeftLowerLeg);
    leftLowerLegNode.x = rotation.x;
}

let rightUpperArmRotation = {
    y: 0,
    z: -(Math.PI / 2 - 0.35)
};

function updateRightUpperArmRotation(rotation) {
    const rightUpperArmNode = vrmManager.rotation(Bone.RightUpperArm);
    rightUpperArmNode.y = rotation.y;
    rightUpperArmNode.z = rotation.z;
}

let rightLowerArmRotation = {
    y: 0,
    z: -0.15
};

function updateRightLowerArmRotation(rotation) {
    const rightLowerArmNode = vrmManager.rotation(Bone.RightLowerArm);
    rightLowerArmNode.y = rotation.y;
    rightLowerArmNode.z = rotation.z;
}

let leftUpperArmRotation = {
    y: 0,
    z: Math.PI / 2 - 0.35
};

function updateLeftUpperArmRotation(rotation) {
    const leftUpperArmNode = vrmManager.rotation(Bone.LeftUpperArm);
    leftUpperArmNode.y = rotation.y;
    leftUpperArmNode.z = rotation.z;
}

let leftLowerArmRotation = {
    y: 0,
    z: 0.15
};

function updateLeftLowerArmRotation(rotation) {
    const leftLowerArmNode = vrmManager.rotation(Bone.LeftLowerArm);
    leftLowerArmNode.y = rotation.y;
    leftLowerArmNode.z = rotation.z;
}

let spineRotation = {
    x: 0,
    y: 0,
    z: 0
};

function updateSpineRotation(rotation) {
    const chestNode = vrmManager.rotation(Bone.Spine);
    chestNode.x = rotation.x;
    chestNode.y = rotation.y;
    chestNode.z = rotation.z;
}

let everyNode = (index) => { //Index為該幀幀數 有些部位不需要補間動畫 只需要擺放相對應轉動角度及張開程度

    { //hips position
        const y = animation[index].hipPosition.y;

        // if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
        //     const y1 = animation[index + 1].hipPosition.y;
        //     const duration = animation[index + 1].time.Time - animation[index].time.Time;
        //     console.log(animation[index].hipPosition.y)
        //     vrmManager.tween(hipsPosition, {
        //         y: y,
        //     }, () => { updateHipsPosition(hipsPosition); }, "hipsPosition", {
        //         y: y1,
        //     }, duration);
        // } else {
        hipsPosition = {
            y: y,
        };
        updateHipsPosition(hipsPosition)
            // }

    }



    // right blink
    {
        allVrms[0].blendShapeProxy._blendShapeGroups.Blink_R.weight = animation[index].eye.rightBlink;
    }
    // left blink
    {
        allVrms[0].blendShapeProxy._blendShapeGroups.Blink_L.weight = animation[index].eye.leftBlink;
    }
    //eyes
    {
        vrmManager.setLookAtTarget(animation[index].eye.iris.x, animation[index].eye.iris.y);
    }
    // mouth
    {
        allVrms[0].blendShapeProxy._blendShapeGroups.A.weight = animation[index].mouth.A;
        allVrms[0].blendShapeProxy._blendShapeGroups.E.weight = animation[index].mouth.E;
        allVrms[0].blendShapeProxy._blendShapeGroups.I.weight = animation[index].mouth.I;
        allVrms[0].blendShapeProxy._blendShapeGroups.O.weight = animation[index].mouth.O;
    }
    // neck
    {
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.x = animation[index].neck.x;
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.y = animation[index].neck.y;
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.z = animation[index].neck.z;
    }
    // spine
    {
        const x = animation[index].spine.x;
        const y = animation[index].spine.y;
        const z = animation[index].spine.z;



        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const x1 = animation[index + 1].spine.x;
            const y1 = animation[index + 1].spine.y;
            const z1 = animation[index + 1].spine.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;

            vrmManager.tween(spineRotation, {
                x: x,
                y: y,
                z: z
            }, () => { updateSpineRotation(spineRotation) }, "spine", {
                x: x1,
                y: y1,
                z: z1
            }, duration);
        } else {
            spineRotation = {
                x: x,
                y: y,
                z: z
            };
            updateSpineRotation(spineRotation)
        }


    }


    // rightUpperArm
    {
        const y = animation[index].rightUpperArm.y;
        const z = animation[index].rightUpperArm.z;

        // let y1 = 0;
        // let z1 = -(Math.PI / 2 - 0.35);
        // let duration = 1;



        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const y1 = animation[index + 1].rightUpperArm.y;
            const z1 = animation[index + 1].rightUpperArm.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(rightUpperArmRotation, {
                y: y,
                z: z
            }, () => { updateRightUpperArmRotation(rightUpperArmRotation) }, "rightUpperArm", {
                y: y1,
                z: z1
            }, duration);
        } else {
            rightUpperArmRotation = {
                y: y,
                z: z
            }
            updateRightUpperArmRotation(rightUpperArmRotation)
        }






    }
    //rightLowerArm
    {
        const y = animation[index].rightLowerArm.y;
        const z = animation[index].rightLowerArm.z;


        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const y1 = animation[index + 1].rightLowerArm.y;
            const z1 = animation[index + 1].rightLowerArm.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(rightLowerArmRotation, {
                y: y,
                z: z
            }, () => { updateRightLowerArmRotation(rightLowerArmRotation) }, "rightLowerArm", {
                y: y1,
                z: z1
            }, duration);
        } else {
            rightLowerArmRotation = {
                y: y,
                z: z
            }
            updateRightLowerArmRotation(rightLowerArmRotation)
        }





    }
    // leftUpperArm
    {
        const y = animation[index].leftUpperArm.y;
        const z = animation[index].leftUpperArm.z;

        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const y1 = animation[index + 1].leftUpperArm;
            const z1 = animation[index + 1].leftUpperArm;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(leftUpperArmRotation, {
                y: y,
                z: z
            }, () => { updateLeftUpperArmRotation(leftUpperArmRotation) }, "leftUpperArm", {
                y: y1,
                z: z1
            }, duration);
        } else {
            leftUpperArmRotation = {
                y: y,
                z: z
            }
            updateLeftUpperArmRotation(leftUpperArmRotation)
        }

    }
    //leftLowerArm
    {
        const y = animation[index].leftLowerArm.y;
        const z = animation[index].leftLowerArm.z;



        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const y1 = animation[index + 1].leftLowerArm.y;
            const z1 = animation[index + 1].leftLowerArm.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(leftLowerArmRotation, {
                y: y,
                z: z
            }, () => { updateLeftLowerArmRotation(leftLowerArmRotation) }, "leftLowerArm", {
                y: y1,
                z: z1
            }, duration);
        } else {
            leftLowerArmRotation = {
                y: y,
                z: z
            };
            updateLeftLowerArmRotation(leftLowerArmRotation)
        }




    }






    //rightHand (wrist)
    {
        allVrms[0].humanoid.humanBones.rightHand[0].node.rotation.z = animation[index].rightHand.z;
    }
    //leftHand
    {
        allVrms[0].humanoid.humanBones.leftHand[0].node.rotation.z = animation[index].leftHand.z;
    }



    //finger
    //right thumb
    {
        allVrms[0].humanoid.humanBones.rightThumbProximal[0].node.rotation.y = animation[index].rightThumb.proximal.y;

        allVrms[0].humanoid.humanBones.rightThumbIntermediate[0].node.rotation.y = animation[index].rightThumb.intermediate.y;

        allVrms[0].humanoid.humanBones.rightThumbDistal[0].node.rotation.y = animation[index].rightThumb.distal.y;
    }
    //right index
    {
        allVrms[0].humanoid.humanBones.rightIndexProximal[0].node.rotation.x = animation[index].rightIndex.proximal.x;
        allVrms[0].humanoid.humanBones.rightIndexProximal[0].node.rotation.z = animation[index].rightIndex.proximal.z;

        allVrms[0].humanoid.humanBones.rightIndexIntermediate[0].node.rotation.z = animation[index].rightIndex.intermediate.z;

        allVrms[0].humanoid.humanBones.rightIndexDistal[0].node.rotation.z = animation[index].rightIndex.distal.z;
    }
    //right middle
    {
        allVrms[0].humanoid.humanBones.rightMiddleProximal[0].node.rotation.z = animation[index].rightMiddle.proximal.z;

        allVrms[0].humanoid.humanBones.rightMiddleIntermediate[0].node.rotation.z = animation[index].rightMiddle.intermediate.z;

        allVrms[0].humanoid.humanBones.rightMiddleDistal[0].node.rotation.z = animation[index].rightMiddle.distal.z;
    }
    //right ring
    {
        allVrms[0].humanoid.humanBones.rightRingProximal[0].node.rotation.x = animation[index].rightRing.proximal.x;
        allVrms[0].humanoid.humanBones.rightRingProximal[0].node.rotation.z = animation[index].rightRing.proximal.z;

        allVrms[0].humanoid.humanBones.rightRingIntermediate[0].node.rotation.z = animation[index].rightRing.intermediate.z;

        allVrms[0].humanoid.humanBones.rightRingDistal[0].node.rotation.z = animation[index].rightRing.distal.z;
    }
    //right little
    {
        allVrms[0].humanoid.humanBones.rightLittleProximal[0].node.rotation.x = animation[index].rightLittle.proximal.x;
        allVrms[0].humanoid.humanBones.rightLittleProximal[0].node.rotation.z = animation[index].rightLittle.proximal.z;

        allVrms[0].humanoid.humanBones.rightLittleIntermediate[0].node.rotation.z = animation[index].rightLittle.intermediate.z;

        allVrms[0].humanoid.humanBones.rightLittleDistal[0].node.rotation.z = animation[index].rightLittle.distal.z;
    }

    //left thumb
    {
        allVrms[0].humanoid.humanBones.leftThumbProximal[0].node.rotation.y = animation[index].leftThumb.proximal.y;

        allVrms[0].humanoid.humanBones.leftThumbIntermediate[0].node.rotation.y = animation[index].leftThumb.intermediate.y;

        allVrms[0].humanoid.humanBones.leftThumbDistal[0].node.rotation.y = animation[index].leftThumb.distal.y;
    }
    //left index
    {
        allVrms[0].humanoid.humanBones.leftIndexProximal[0].node.rotation.x = animation[index].leftIndex.proximal.x;
        allVrms[0].humanoid.humanBones.leftIndexProximal[0].node.rotation.z = animation[index].leftIndex.proximal.z;

        allVrms[0].humanoid.humanBones.leftIndexIntermediate[0].node.rotation.z = animation[index].leftIndex.intermediate.z;

        allVrms[0].humanoid.humanBones.leftIndexDistal[0].node.rotation.z = animation[index].leftIndex.distal.z;
    }
    //left middle
    {
        allVrms[0].humanoid.humanBones.leftMiddleProximal[0].node.rotation.z = animation[index].leftMiddle.proximal.z;

        allVrms[0].humanoid.humanBones.leftMiddleIntermediate[0].node.rotation.z = animation[index].leftMiddle.intermediate.z;

        allVrms[0].humanoid.humanBones.leftMiddleDistal[0].node.rotation.z = animation[index].leftMiddle.distal.z;
    }
    //left ring
    {
        allVrms[0].humanoid.humanBones.leftRingProximal[0].node.rotation.x = animation[index].leftRing.proximal.x;
        allVrms[0].humanoid.humanBones.leftRingProximal[0].node.rotation.z = animation[index].leftRing.proximal.z;

        allVrms[0].humanoid.humanBones.leftRingIntermediate[0].node.rotation.z = animation[index].leftRing.intermediate.z;

        allVrms[0].humanoid.humanBones.leftRingDistal[0].node.rotation.z = animation[index].leftRing.distal.z;
    }
    //right little
    {
        allVrms[0].humanoid.humanBones.leftLittleProximal[0].node.rotation.x = animation[index].leftLittle.proximal.x;
        allVrms[0].humanoid.humanBones.leftLittleProximal[0].node.rotation.z = animation[index].leftLittle.proximal.z;

        allVrms[0].humanoid.humanBones.leftLittleIntermediate[0].node.rotation.z = animation[index].leftLittle.intermediate.z;

        allVrms[0].humanoid.humanBones.leftLittleDistal[0].node.rotation.z = animation[index].leftLittle.distal.z;
    }
    // rightUpperLeg
    {
        const x = animation[index].rightUpperLeg.x;
        const z = animation[index].rightUpperLeg.z;

        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const x1 = animation[index + 1].rightUpperLeg.x; //下一幀JSON檔資料中的該部位旋轉角度
            const z1 = animation[index + 1].rightUpperLeg.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time; //
            vrmManager.tween(rightUpperLegRotation, {
                x: x,
                z: z
            }, () => { updateRightUpperLegRotation(rightUpperLegRotation) }, "rightUpperLeg", {
                x: x1,
                z: z1
            }, duration);
        } else {
            rightUpperLegRotation = {
                x: x,
                z: z
            };
            updateRightUpperLegRotation(rightUpperLegRotation);
        }




    }
    //rightLowerLeg
    {
        const x = animation[index].rightLowerLeg.x;



        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const x1 = animation[index + 1].rightLowerLeg.x;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(rightLowerLegRotation, {
                x: x
            }, () => { updateRightLowerLegRotation(rightLowerLegRotation) }, "rightLowerLeg", {
                x: x1
            }, duration);
        } else {
            rightLowerLegRotation = {
                x: x
            };
            updateRightLowerLegRotation(rightLowerLegRotation);
        }



    }
    // leftUpperLeg
    {
        const x = animation[index].leftUpperLeg.x;
        const z = animation[index].leftUpperLeg.z;

        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const x1 = animation[index + 1].leftUpperLeg.x;
            const z1 = animation[index + 1].leftUpperLeg.z;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(leftUpperLegRotation, {
                x: x,
                z: z
            }, () => { updateLeftUpperLegRotation(leftUpperLegRotation) }, "leftUpperLeg", {
                x: x1,
                z: z1
            }, duration);
        } else {
            leftUpperLegRotation = {
                x: x,
                z: z
            };
            updateLeftUpperLegRotation(leftUpperLegRotation)
        }



    }
    //leftLowerLeg
    {
        const x = animation[index].leftLowerLeg.x;

        if (index < animation.length - 1) { //除了最後一frame(index)資料 他下一筆沒有資料了 所以就不跑補間 只跑最後一筆自己的資料
            const x1 = animation[index + 1].leftLowerLeg.x;
            const duration = animation[index + 1].time.Time - animation[index].time.Time;
            vrmManager.tween(leftLowerLegRotation, {
                x: x
            }, () => { updateLeftLowerLegRotation(leftLowerLegRotation) }, "leftLowerLeg", {
                x: x1
            }, duration);
        } else {
            leftLowerLegRotation = {
                x: x
            }
            updateLeftLowerLegRotation(leftLowerLegRotation);
        }

    }
}


function resetTweenData() { //按stop按鈕跟影片播放結束時 補間動畫資料需要先還原 下次播放的時候 才不會從最後停止的動作開始做
    hipsPosition = {
        y: 0
    };


    rightUpperLegRotation = {
        x: 0,
        z: 0
    };



    rightLowerLegRotation = {
        x: 0
    };


    leftUpperLegRotation = {
        x: 0,
        z: 0
    };


    leftLowerLegRotation = {
        x: 0
    };


    rightUpperArmRotation = {
        y: 0,
        z: -(Math.PI / 2 - 0.35)
    };



    rightLowerArmRotation = {
        y: 0,
        z: -0.15
    };



    leftUpperArmRotation = {
        y: 0,
        z: Math.PI / 2 - 0.35
    };


    leftLowerArmRotation = {
        y: 0,
        z: 0.15
    };


    spineRotation = {
        x: 0,
        y: 0,
        z: 0
    };


}







//-----------------------------------------------------------------------------------------------

// function forward() {
//     let tempTime = clock.elapsedTime - 5;
//     if (tempTime < 0) {
//         tempTime = 0;
//         positionReset(); //讓vrm擺出相對應姿勢
//         frame = 0;
//     } else {
//         for (let i = frame - 1; i >= 0; i--) { //frame 在animate()判斷下面時已經+1了 要先-1才是現在目前的frame   ->往回五秒後 從還沒往回五秒時侯的目前那一frame去往回找現在時間大於哪一frame的時間 去跑該frame 找到之後break
//             if (tempTime >= animation[i].time.Time) {
//                 frame = i;
//                 break; //break出此for
//             }
//         }

//         setPosition(frame); //讓vrm擺出相對應姿勢
//     }

//     clock.elapsedTime = tempTime; //讓現在時間變成五秒前

//     if (playStatus == 0 && newBeginStatus == 0) { //暫停按forward的時候
//         stopTime = clock.elapsedTime;
//     }
//     const time = clock.elapsedTime;
//     calculateTime(time.toFixed(2), "minute", "second"); //顯示往回五秒前的時間


//     if (endStatus == 1) { //影片完全播完的時候 按forward json會回去繼續播放 跑animate
//         playStatus = 1; //要讓他可以跑進animate()
//         endStatus = 0;
//         newBeginStatus = 0;

//         clock.start();
//         clock.elapsedTime = tempTime;

//         const deltaTime = clock.getDelta();
//         allVrms[0].update(deltaTime);

//         animate();
//     }
// }

// function backward() {    
//     let tempTime = clock.elapsedTime + 5;
//     if (tempTime > animation[animation.length - 1].time.Time) {
//         tempTime = animation[animation.length - 1].time.Time;
//         frame = animation.length - 1;
//     } else {
//         for (let i = frame - 1; i <= animation.length - 1; i++) { //frame 在animate()判斷下面時已經+1了 要先-1才是現在目前的frame   ->往後五秒後 從還沒往後五秒時侯的目前那一frame去往後找現在時間小於哪一frame的時間 去跑該frame的前一個frame 找到之後break
//             if (i == -1) { //讓json檔剛拖進(frame=0) 還沒按play時 按backward可以成功讓他跑起來
//                 continue;
//             }
//             if (tempTime <= animation[i].time.Time) {
//                 frame = i - 1;
//                 break;
//             }
//         }
//     }
//     setPosition(frame); //讓vrm擺出相對應姿勢

//     clock.elapsedTime = tempTime; //讓現在時間變成五秒後
//     if (playStatus == 0 && newBeginStatus == 0) { //暫停按backward的時候
//         stopTime = clock.elapsedTime;
//     }
//     const time = clock.elapsedTime;
//     calculateTime(time.toFixed(2), "minute", "second"); //顯示往後五秒的時間

//     if (newBeginStatus == 1 && playStatus == 0 && endStatus == 0) { //按stop按鈕完按backward的情況
//         playStatus = 1; //要讓他可以跑進animate()
//         newBeginStatus = 0;

//         clock.start();
//         clock.elapsedTime = 5;

//         const deltaTime = clock.getDelta();
//         allVrms[0].update(deltaTime);

//         animate();
//     }
// }

function setPosition(index) {
    {
        allVrms[0].humanoid.humanBones.hips[0].node.position.y = animation[index].hipPosition.y + adjustHipPosition;
        console.log("setPosition" + allVrms[0].humanoid.humanBones.hips[0].node.position.y)
    }

    {
        allVrms[0].blendShapeProxy._blendShapeGroups.Blink_R.weight = animation[index].eye.rightBlink
    }
    // left blink
    {
        allVrms[0].blendShapeProxy._blendShapeGroups.Blink_L.weight = animation[index].eye.leftBlink
    }
    //eyes
    {
        lookAtTarget.position.x = animation[index].eye.iris.x
        lookAtTarget.position.y = animation[index].eye.iris.y
    }
    // mouth
    {
        allVrms[0].blendShapeProxy._blendShapeGroups.A.weight = animation[index].mouth.A;
        allVrms[0].blendShapeProxy._blendShapeGroups.E.weight = animation[index].mouth.E;
        allVrms[0].blendShapeProxy._blendShapeGroups.I.weight = animation[index].mouth.I;
        allVrms[0].blendShapeProxy._blendShapeGroups.O.weight = animation[index].mouth.O;
    }
    // neck
    {
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.x = animation[index].neck.x
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.y = animation[index].neck.y
        allVrms[0].humanoid.humanBones.neck[0].node.rotation.z = animation[index].neck.z
            // console.log(index)
            //animation ?
    }
    // spine
    {
        allVrms[0].humanoid.humanBones.spine[0].node.rotation.x = animation[index].spine.x
        allVrms[0].humanoid.humanBones.spine[0].node.rotation.y = animation[index].spine.y
        allVrms[0].humanoid.humanBones.spine[0].node.rotation.z = animation[index].spine.z

    }

    //rightUpperArm
    {
        allVrms[0].humanoid.humanBones.rightUpperArm[0].node.rotation.y = animation[index].rightUpperArm.y
        allVrms[0].humanoid.humanBones.rightUpperArm[0].node.rotation.z = animation[index].rightUpperArm.z
    }
    //rightLowerArm
    {
        allVrms[0].humanoid.humanBones.rightLowerArm[0].node.rotation.y = animation[index].rightLowerArm.y
        allVrms[0].humanoid.humanBones.leftLowerArm[0].node.rotation.z = animation[index].leftLowerArm.z;


    }
    //leftUpperArm
    {
        allVrms[0].humanoid.humanBones.leftUpperArm[0].node.rotation.y = animation[index].leftUpperArm.y
        allVrms[0].humanoid.humanBones.leftUpperArm[0].node.rotation.z = animation[index].leftUpperArm.z
    }
    //leftLowerArm
    {
        allVrms[0].humanoid.humanBones.leftLowerArm[0].node.rotation.y = animation[index].leftLowerArm.y
        allVrms[0].humanoid.humanBones.leftLowerArm[0].node.rotation.z = animation[index].leftLowerArm.z;
    }


    //rightHand (wrist)
    {
        allVrms[0].humanoid.humanBones.rightHand[0].node.rotation.z = animation[index].rightHand.z
    }
    //leftHand
    {
        allVrms[0].humanoid.humanBones.leftHand[0].node.rotation.z = animation[index].leftHand.z
    }
    //finger
    //right thumb
    {
        allVrms[0].humanoid.humanBones.rightThumbProximal[0].node.rotation.y = animation[index].rightThumb.proximal.y;

        allVrms[0].humanoid.humanBones.rightThumbIntermediate[0].node.rotation.y = animation[index].rightThumb.intermediate.y;

        allVrms[0].humanoid.humanBones.rightThumbDistal[0].node.rotation.y = animation[index].rightThumb.distal.y;
    }
    //right index
    {
        allVrms[0].humanoid.humanBones.rightIndexProximal[0].node.rotation.x = animation[index].rightIndex.proximal.x;
        allVrms[0].humanoid.humanBones.rightIndexProximal[0].node.rotation.z = animation[index].rightIndex.proximal.z;

        allVrms[0].humanoid.humanBones.rightIndexIntermediate[0].node.rotation.z = animation[index].rightIndex.intermediate.z;

        allVrms[0].humanoid.humanBones.rightIndexDistal[0].node.rotation.z = animation[index].rightIndex.distal.z;
    }
    //right middle
    {
        allVrms[0].humanoid.humanBones.rightMiddleProximal[0].node.rotation.z = animation[index].rightMiddle.proximal.z;

        allVrms[0].humanoid.humanBones.rightMiddleIntermediate[0].node.rotation.z = animation[index].rightMiddle.intermediate.z;

        allVrms[0].humanoid.humanBones.rightMiddleDistal[0].node.rotation.z = animation[index].rightMiddle.distal.z;
    }
    //right ring
    {
        allVrms[0].humanoid.humanBones.rightRingProximal[0].node.rotation.x = animation[index].rightRing.proximal.x;
        allVrms[0].humanoid.humanBones.rightRingProximal[0].node.rotation.z = animation[index].rightRing.proximal.z;

        allVrms[0].humanoid.humanBones.rightRingIntermediate[0].node.rotation.z = animation[index].rightRing.intermediate.z;

        allVrms[0].humanoid.humanBones.rightRingDistal[0].node.rotation.z = animation[index].rightRing.distal.z;
    }
    //right little
    {
        allVrms[0].humanoid.humanBones.rightLittleProximal[0].node.rotation.x = animation[index].rightLittle.proximal.x;
        allVrms[0].humanoid.humanBones.rightLittleProximal[0].node.rotation.z = animation[index].rightLittle.proximal.z;

        allVrms[0].humanoid.humanBones.rightLittleIntermediate[0].node.rotation.z = animation[index].rightLittle.intermediate.z;

        allVrms[0].humanoid.humanBones.rightLittleDistal[0].node.rotation.z = animation[index].rightLittle.distal.z;
    }

    //left thumb
    {
        allVrms[0].humanoid.humanBones.leftThumbProximal[0].node.rotation.y = animation[index].leftThumb.proximal.y;

        allVrms[0].humanoid.humanBones.leftThumbIntermediate[0].node.rotation.y = animation[index].leftThumb.intermediate.y;

        allVrms[0].humanoid.humanBones.leftThumbDistal[0].node.rotation.y = animation[index].leftThumb.distal.y;
    }
    //left index
    {
        allVrms[0].humanoid.humanBones.leftIndexProximal[0].node.rotation.x = animation[index].leftIndex.proximal.x;
        allVrms[0].humanoid.humanBones.leftIndexProximal[0].node.rotation.z = animation[index].leftIndex.proximal.z;

        allVrms[0].humanoid.humanBones.leftIndexIntermediate[0].node.rotation.z = animation[index].leftIndex.intermediate.z;

        allVrms[0].humanoid.humanBones.leftIndexDistal[0].node.rotation.z = animation[index].leftIndex.distal.z;
    }
    //left middle
    {
        allVrms[0].humanoid.humanBones.leftMiddleProximal[0].node.rotation.z = animation[index].leftMiddle.proximal.z;

        allVrms[0].humanoid.humanBones.leftMiddleIntermediate[0].node.rotation.z = animation[index].leftMiddle.intermediate.z;

        allVrms[0].humanoid.humanBones.leftMiddleDistal[0].node.rotation.z = animation[index].leftMiddle.distal.z;
    }
    //left ring
    {
        allVrms[0].humanoid.humanBones.leftRingProximal[0].node.rotation.x = animation[index].leftRing.proximal.x;
        allVrms[0].humanoid.humanBones.leftRingProximal[0].node.rotation.z = animation[index].leftRing.proximal.z;

        allVrms[0].humanoid.humanBones.leftRingIntermediate[0].node.rotation.z = animation[index].leftRing.intermediate.z;

        allVrms[0].humanoid.humanBones.leftRingDistal[0].node.rotation.z = animation[index].leftRing.distal.z;
    }
    //right little
    {
        allVrms[0].humanoid.humanBones.leftLittleProximal[0].node.rotation.x = animation[index].leftLittle.proximal.x;
        allVrms[0].humanoid.humanBones.leftLittleProximal[0].node.rotation.z = animation[index].leftLittle.proximal.z;

        allVrms[0].humanoid.humanBones.leftLittleIntermediate[0].node.rotation.z = animation[index].leftLittle.intermediate.z;

        allVrms[0].humanoid.humanBones.leftLittleDistal[0].node.rotation.z = animation[index].leftLittle.distal.z;
    }

    //rightUpperLeg
    {
        allVrms[0].humanoid.humanBones.rightUpperLeg[0].node.rotation.x = animation[index].rightUpperLeg.x
        allVrms[0].humanoid.humanBones.rightUpperLeg[0].node.rotation.z = animation[index].rightUpperLeg.z
    }
    //rightLowerLeg
    {
        allVrms[0].humanoid.humanBones.rightLowerLeg[0].node.rotation.x = animation[index].rightLowerLeg.x
    }
    //leftUpperLeg
    {
        allVrms[0].humanoid.humanBones.leftUpperLeg[0].node.rotation.x = animation[index].leftUpperLeg.x
        allVrms[0].humanoid.humanBones.leftUpperLeg[0].node.rotation.z = animation[index].leftUpperLeg.z
    }
    //leftLowerLeg
    {
        allVrms[0].humanoid.humanBones.leftLowerLeg[0].node.rotation.x = animation[index].leftLowerLeg.x
    }
}