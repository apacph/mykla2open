function undetectedAnimation() {
    const warning = document.getElementById('warning');
    warning.style.visibility = "visible";
    const number = document.getElementById('number');
    number.className = "undetectedText";
    number.style.fontSize = "30px";
    number.innerHTML = "User <br> NOT Detected";
}

function detectedAnimation() {
    defaultAnimation();
    const warning = document.getElementById('warning');
    if (animation.length > 0) {
        warning.title = "Please play the JSON file"
    } else {
        stopWarning();
    }
}

function stopWarning() {
    const warning = document.getElementById('warning');
    warning.style.visibility = "hidden";
    warning.title = "";
}