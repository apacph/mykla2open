function compareHead(face, standard) {
    /*
        roll:左右晃(z軸)
        yaw: 轉頭(y軸)
        pitch:點頭(x軸)
        左右以螢幕上畫面為準(螢幕畫面右臉=人的左臉)
    */
    return new Promise((resolve) => {
        let finalScore = 0;
        if(face != undefined){
            $.ajax({
                url: "http://localhost:57501/headRotate",
                data: JSON.stringify({
                    faceRight: face[356],
                    faceLeft: face[127],
                    faceTop: face[10],
                    faceBottom: face[200]
                }),
                dataType: "json",
                contentType: "application/json;  charset=utf-8",
                type: "POST",
                success: function(detectedHead) {
                    if (detectedHead == undefined) {
                        resolve(0);
                    }
                    const deltaX = Math.abs(detectedHead.pitch - standard.x);
                    const deltaY = Math.abs(detectedHead.yaw - standard.y);
                    const deltaZ = Math.abs(detectedHead.roll - standard.z);
                    const delta = (deltaX + deltaY + deltaZ) / 3;
                    finalScore = calcScore(delta, 0.1, 0.15, 0.25);
                    resolve(finalScore);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log("error");
                }
            });
        }
        else{
            resolve(0);
        }
    });
}

// function calcHeadScore(detected, standard) {
//     let finalScore = 0;
//     if (delta <= 0.1) {
//         finalScore = 3;
//     } else if (delta <= 0.15) {
//         finalScore = 2;
//     } else if (delta <= 0.25) {
//         finalScore = 1;
//     } else {
//         finalScore = 0;
//     }
//     return finalScore;
// }