function compareArm(mult, part, pose, hand, standard){
    return new Promise(async(resolve) => {
        let shoulder;
        let elbow;
        let armScore;
        if(hand==undefined || pose==undefined){
            resolve(0);
        }
        else{
            let wrist = hand[0];
            if(mult==1){
                shoulder = pose[12];
                elbow = pose[14];
            }
            else if(mult==-1){
                shoulder = pose[11];
                elbow = pose[13];
            }

            let shoulderPoint = pointToVec(shoulder);
            let elbowPoint = pointToVec(elbow);
            let wristPoint = pointToVec(wrist);
            let armAngle = getAngle(shoulderPoint, elbowPoint, wristPoint)

            if(mult==1){
                if ((elbow.y > shoulder.y && armAngle <= 0.25 && elbow.x < shoulder.x) || armAngle > 0.8) {
                    armScore = await armOutDetection(mult, part, pose, hand, standard);
                } else {
                    armScore = await armInDetection(mult, part, pose, hand, standard);
                }
            }
            else if(mult==-1){
                if ((elbow.y > shoulder.y && armAngle <= 0.25 && elbow.x > shoulder.x) || armAngle > 0.8) {
                    armScore = await armOutDetection(mult, part, pose, hand, standard);
                } else {
                    armScore = await armInDetection(mult, part, pose, hand, standard);
                }
            }
            resolve(armScore);
        }
        
    })
}
function armInDetection(mult, part, pose, hand, standard) {
    return new Promise(async(resolve) => {
        let armInScore;
        if (pose != undefined && hand != undefined) {
            let shoulder;
            let elbow;
            let wrist = hand[0];

            if (mult == 1) {
                shoulder = pose[12];
                elbow = pose[14]
            } else if (mult == -1) {
                shoulder = pose[11];
                elbow = pose[13]
            }
            armInScore = await getDetectedArmIn(mult, shoulder, elbow, wrist, part, standard);
            resolve(armInScore);
        }
        else{
            resolve(0);
        }
    })
}

function getDetectedArmIn(mult, shoulder, elbow, wrist, part, standard){
    return new Promise((resolve) => {
        $.ajax({
            url: "http://localhost:57501/armInRotate",
            data: JSON.stringify({
                mult: mult,
                shoulder: shoulder,
                elbow: elbow,
                wrist: wrist
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            success: function(returnData) {
                let armInScore = calcArmInAngle(part, standard, returnData, mult);
                resolve(armInScore);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });
    });
}

function calcArmInAngle(part, standard, returnData, mult){
    let armInScore = 0;
    if(part=="upper"){
        let upperScore = 0;
        upperScore += Math.abs(standard.y - returnData.upperY);
        upperScore += Math.abs(standard.z - returnData.upperZ);
        upperScore /= 2;
        armInScore = calcScore(upperScore, 0.45, 0.7, 0.95);
    }
    else if(part=="lower"){
        let lowerScore = 0;
        lowerScore += Math.abs(standard.y - returnData.lowerY);
        lowerScore += Math.abs(standard.z - returnData.lowerZ);
        armInScore = calcScore(lowerScore, 0.45, 0.7, 0.95);
    }
    return armInScore;
}

// function calcArmInScore(score){
//     if(score < 0.45){
//         return 3;
//     }
//     else if(score >= 0.45 && score < 0.7){
//         return 2;
//     }
//     else if(score >= 0.7 && score < 0.95){
//         return 1;
//     }
//     else{
//         return 0;
//     }
// }