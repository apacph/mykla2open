function compareFinger(side, fingerNum, hand, standard) {
    return new Promise((resolve) => {

        if (hand == undefined) {
            resolve(0);
        }

        const fingers = ["Thumb", "Index", "Middle", "Ring", "Little"];
        const finger = fingers[fingerNum];
        //fingers: Thumb,Index,Middle,Index,Little
        //fingerNum: 0  , 1   ,  2    ,  3 ,  4  
        const startIndex = fingerNum * 4;
        const fingerLandmark = [hand[startIndex + 1], hand[startIndex + 2], hand[startIndex + 3], hand[startIndex + 4]];

        const wristLandmark = hand[0];
        const midFinBottom = hand[9];

        //各階段的最大誤差限制
        const rangeLimit = {
            Thumb: {
                excellent: 0.2,
                good: 0.35,
                ok: 0.5
            },
            Index: {
                excellent: 0.1,
                good: 0.15,
                ok: 0.3
            },
            Middle: {
                excellent: 0.15,
                good: 0.3,
                ok: 0.55
            },
            Ring: {
                excellent: 0.15,
                good: 0.25,
                ok: 0.4
            },
            Little: {
                excellent: 0.3,
                good: 0.4,
                ok: 0.45
            }

        }


        $.ajax({
            url: "http://localhost:57501/fingerRotate",
            data: JSON.stringify({
                side: side,
                finger: finger,
                wristLandmark: wristLandmark,
                midFinBottomLandmark: midFinBottom,
                fingerLandmark: fingerLandmark
            }),
            dataType: "json",
            contentType: "application/json;  charset=utf-8",
            type: "POST",
            success: function(returnData) {
                const detectedFinger = returnData.rotations;
                let delta = 0;
                const axis = finger === "Thumb" ? "y" : "z";
                const deltaProximalAxis = Math.abs(standard.proximal[axis] - detectedFinger[0]);
                const deltaIntermediateAxis = Math.abs(standard.intermediate[axis] - detectedFinger[1]);
                const deltaDistalAxis = Math.abs(standard.distal[axis] - detectedFinger[2]);
                delta = deltaProximalAxis + deltaIntermediateAxis + deltaDistalAxis;
                if (finger !== "Middle" && finger !== "Thumb") {
                    const deltaProximalX = Math.abs(standard.proximal.x - detectedFinger[3]);
                    delta += deltaProximalX;
                    delta /= 4;
                } else {
                    delta /= 3;
                }
                let finalDelta = delta;
                const excellentLimit = rangeLimit[finger].excellent;
                const goodLimit = rangeLimit[finger].good;
                const okLimit = rangeLimit[finger].ok;
                const score = calcScore(finalDelta, excellentLimit, goodLimit, okLimit);
                resolve(score);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });
    });

}