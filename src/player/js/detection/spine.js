//0~1 +3 1~2 +2
//pose = results.poseLandmarks
//standard = json檔裡面被標記部位的計算值
function compareSpine(pose, standard) {
    return new Promise(async(resolve) => {
        if (pose != undefined) {
            const resultPoseLandmarks = pose;
            /*
            roll:左右晃(z軸)
            yaw: 轉身(y軸)
            pitch:鞠躬(x軸)
            */
            const detected = await getDetectedSpine(resultPoseLandmarks);

            let spineScore = 0;
            let score = 0;
            score += Math.abs(detected.pitch - standard.x);
            score += Math.abs(detected.yaw - standard.y);
            score += Math.abs(detected.roll - standard.z);
            score /= 3;
            spineScore = calcScore(score, 0.125, 0.187, 0.25);
            resolve(spineScore);
        }
        else{ //若沒偵測到 就給他0分
            resolve(0);
        }
    })

}

function getDetectedSpine(resultPoseLandmarks){
    return new Promise((resolve) => {
        $.ajax({
            url: "http://127.0.0.1:57501/spineRotate",
            data: JSON.stringify({
                leftHip: resultPoseLandmarks[23],
                rightHip: resultPoseLandmarks[24],
                rightShoulder: resultPoseLandmarks[12],
                leftShoulder: resultPoseLandmarks[11],
            }),
            dataType: "json",
            contentType: "application/json;  charset=utf-8",
            type: "POST",
            success: function(returnData) {
                resolve(returnData);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });
    })
}
