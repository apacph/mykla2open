function compareLeg(side,part,pose,standard) {
    return new Promise((resolve) => {
        let legScore = 0;
        if (pose != undefined) {
            $.ajax({
                url: "http://localhost:57501/legRotate",
                data: JSON.stringify({
                    leftHip: pose[23],
                    leftKnee: pose[25],
                    leftAnkle: pose[27],
                    rightHip: pose[24],
                    rightKnee: pose[26],
                    rightAnkle: pose[28]
                }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                type: "POST",
                success: function(returnData) {
                    legScore = calcLegAngle(returnData,side,part,standard);
                    resolve(legScore);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log("error");
                }
            });
        }
        else{
            resolve(0);
        }
    })
}

function calcLegAngle(returnData,side,part,standard){
    let legScore = 0;
    if(side=="right"){
        if(part=="upper"){
            let upperScore = 0;
            upperScore += Math.abs(standard.x - returnData.rightUpperX);
            upperScore += Math.abs(standard.z - returnData.rightUpperZ);
            upperScore /= 2;
            legScore = calcScore(upperScore, 0.5, 0.65, 0.9)
        }
        else if(part=="lower"){
            let lowerScore = 0;
            lowerScore += Math.abs(standard.x - returnData.rightLowerX);
            legScore = calcScore(lowerScore, 0.5, 0.65, 0.9)
        }
    }
    else if(side=="left"){
        if(part=="upper"){
            let upperScore = 0;
            upperScore += Math.abs(standard.x - returnData.leftUpperX);
            upperScore += Math.abs(standard.z - returnData.leftUpperZ);
            upperScore /= 2;
            legScore = calcScore(upperScore, 0.5, 0.65, 0.9)
        }
        else if(part=="lower"){
            let lowerScore = 0;
            lowerScore += Math.abs(standard.x - returnData.leftLowerX);
            legScore = calcScore(lowerScore, 0.5, 0.65, 0.9)
        }
    }
    return legScore;
}

// function calcLegScore(score){
//     if(score < 0.5){
//         return 3;
//     }
//     else if(score >= 0.5 && score < 0.65){
//         return 2;
//     }
//     else if(score >= 0.65 && score < 0.9){
//         return 1;
//     }
//     else{
//         return 0;
//     }  
// }