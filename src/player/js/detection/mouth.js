//face = results.faceLandmarks
//standard = json檔裡面被標記部位的計算值
function compareMouth(face, standard) {

    if(face != undefined){
        let mouthScore = 0;
        const mouthRight = face[78];
        const mouthLeft = face[308];
        const mouthWidth = distance3d(mouthRight, mouthLeft);


        const mouthTop = face[13];
        const mouthBottom = face[14];
        const mouthHeight = distance3d(mouthTop, mouthBottom);

        const jaw = face[200];
        let topToJaw = distanceP(jaw, mouthTop); //下巴至嘴唇上方距離
        let leftToJaw = distanceP(jaw, mouthLeft); //下巴至左嘴角距離
        let playerEMouthShape = leftToJaw / topToJaw;
        let playerMouthRatio = mouthHeight / mouthWidth;


        if(standard.E > 0){
            console.log("E")
            let eMouthScore = Math.abs(standard.eMouthShape - playerEMouthShape);
            mouthScore = calcScore(eMouthScore, 0.15, 0.175, 0.2);
            // console.log("player" + playerEMouthShape);
            // console.log("difference" + Math.abs(standard.eMouthShape - playerEMouthShape))
            // console.log("data" + standard.eMouthShape);
        }
        else if(standard.A > 0){
            console.log("A")
            let aMouthScore = Math.abs(standard.mouthRatio - playerMouthRatio);
            mouthScore = calcScore(aMouthScore,  0.065, 0.1325, 0.2);
            // console.log("player" + playerMouthRatio);
            // console.log("difference" + Math.abs(standard.mouthRatio - playerMouthRatio))
            // console.log("data" + standard.mouthRatio);
        }
        else if(standard.I > 0){
            console.log("I")
            let iMouthScore = Math.abs(standard.mouthRatio - playerMouthRatio);
            mouthScore = calcScore(iMouthScore,  0.1, 0.15, 0.2);
        }
        else if(standard.O > 0){
            console.log("O")
            let oMouthScore = Math.abs(standard.mouthRatio - playerMouthRatio);
            mouthScore = calcScore(oMouthScore,  0.1, 0.39, 0.645, 0.9);
        }

        return mouthScore;
    }
    else{
        return 0; //若沒偵測到臉 直接把嘴巴偵測分數打0分 (也就是bad
    }
    
}