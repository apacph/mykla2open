function compareHand(mult, pose, hand, standard) {
    return new Promise(async(resolve) => {
        const elbow = mult == 1 ? pose[13] : pose[14];
        let wrist;
        let midFin;
        let thumb;
        let littleFin;
        if (hand != undefined) {
            wrist = hand[0];
            midFin = hand[9]
            thumb = hand[2];
            littleFin = hand[17];
        } else {
            resolve(0);
        }

        const detectedPoint = [elbow, wrist, midFin, thumb, littleFin];
        const detectedHand = await getDetectedHand(detectedPoint, mult);
        if (detectedHand == undefined) {
            const delta = Math.abs(detectedHand.handZ - standard.z);
            const finalScore = calcScore(delta, 1, 2, 4);
            resolve(finalScore);
        } else {
            resolve(0);
        }

    })
}

function getDetectedHand(detectedPoint, mult) {
    return new Promise(resolve => {
        $.ajax({
            url: "http://localhost:57501/handRotate",
            data: JSON.stringify({
                hand: detectedPoint,
                mult: mult
            }),
            dataType: "json",
            contentType: "application/json;  charset=utf-8",
            type: "POST",
            success: function(returnData) {
                resolve(returnData);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });
    })
}
// function calcHandScore(detected, standard) {
//     if (delta <= 1) {
//         return 3;
//     } else if (delta <= 2) {
//         return 2;
//     } else if (delta <= 4) {
//         return 1;
//     } else {
//         return 0;
//     }
// }