/**
 * 眼睛偵測
 * @param {*} face 
 */
function compareEye(face, standard) {
    return new Promise(async(resolve) => {
        if (face) {

            for (let i = 0; i < face.length; i++) {
                const view = face[0].faceInViewConfidence;
                if (view < 1) {
                    //當沒偵測到眼睛
                    break;
                }


                const keypoints = face[i].scaledMesh;

                //ts的faceMesh的left即為本人的左，但下方的left是以vrm為主，所以會相反，座標貼在臉上，x往左(vrm的右)，y往下，z往人(後)
                //下方如未特別標明，則是以vrm為主
                //ts的位置array=>0:x 1:y 2:z

                const rightEye = {
                    //rightEye-左/右眼角(本人的左眼)
                    l: keypoints[362], //vrm右眼近鼻(右眼的左眼角)，本人左眼近鼻眼角(左眼的右眼角)
                    r: keypoints[263], //vrm右眼遠鼻(右眼的右眼角)，本人左眼遠鼻眼角(左眼的左眼角)
                    //rightEye-上/下眼皮
                    uL: keypoints[385], //vrm右眼上眼皮近鼻點(右眼上眼皮中線之左點)，本人左眼上眼皮近鼻點(左眼上眼皮中線之右點)
                    uC: keypoints[386], //vrm右眼上眼皮點(右眼上眼皮中線之中點)，本人左眼上眼皮點(左眼上眼皮中線之中點)
                    uR: keypoints[387], //vrm右眼上眼皮遠鼻點(右眼上眼皮中線之右點)，本人左眼上眼皮遠鼻點(左眼上眼皮中線之左點)
                    dL: keypoints[380], //vrm右眼下眼皮近鼻點(右眼下眼皮中線之左點)，本人左眼下眼皮近鼻點(左眼下眼皮中線之右點)
                    dC: keypoints[374], //vrm右眼下眼皮中點(右眼下眼皮中線之中點)，本人左眼下眼皮點(左眼下眼皮中線之中點)
                    dR: keypoints[373], //vrm右眼下眼皮遠鼻點(右眼下眼皮中線之右點)，本人左眼下眼皮遠鼻點(左眼下眼皮中線之左點)
                }




                const leftEye = {
                    //leftEye-左/右眼角(本人的右眼)
                    l: keypoints[33], //vrm左眼遠鼻(左眼的左眼角)，本人右眼遠鼻眼角(右眼的右眼角)
                    r: keypoints[133], //vrm左眼近鼻(左眼的右眼角)，本人右眼近鼻眼角(右眼的左眼角)
                    //leftEye-上/下眼皮
                    uL: keypoints[158], //vrm左眼上眼皮近鼻點(左眼上眼皮中線之右點)，本人右眼上眼皮近鼻點(右眼上眼皮中線之左點)
                    uC: keypoints[159], //vrm左眼上眼皮中點(左眼上眼皮中線之中點)，本人右眼上眼皮中點(右眼上眼皮中線之中點)
                    uR: keypoints[160], //vrm左眼上眼皮遠鼻點(左眼上眼皮中線之左點)，本人右眼上眼皮遠鼻點(右眼上眼皮中線之右點)
                    dL: keypoints[153], //vrm左眼下眼皮近鼻點(左眼下眼皮中線之右點)，本人右眼下眼皮近鼻點(右眼下眼皮中線之左點)
                    dC: keypoints[145], //vrm左眼下眼皮中點(左眼下眼皮中線之中點)，本人右眼下眼皮中點(右眼下眼皮中線之中點)
                    dR: keypoints[144], //vrm左眼下眼皮遠鼻點(左眼下眼皮中線之左點)，本人右眼下眼皮遠鼻點(右眼下眼皮中線之右點)
                }




                const rightIris = {
                    l: keypoints[469],
                    r: keypoints[471],
                    u: keypoints[470],
                    d: keypoints[472],
                    center: keypoints[468]
                }

                const leftIris = {
                    l: keypoints[476],
                    r: keypoints[474],
                    u: keypoints[477],
                    d: keypoints[475],
                    center: keypoints[473]
                }



                const detected = await getDetectedEye(rightEye, rightIris, leftEye, leftIris);

                const blinkRDelta = Math.abs(standard.rightBlink - detected.blinkR);
                const blinkLDelta = Math.abs(standard.leftBlink - detected.blinkL);
                const irisXDelta = Math.abs(standard.iris.x - detected.lookAtTargetX);
                const irisXAxisDelta = standard.iris.x * detected.lookAtTargetX;
                const irisYDelta = Math.abs(standard.iris.y - detected.lookAtTargetY);
                console.log("blinkLDelta" + ":" + blinkLDelta);
                console.log("blinkRDelta" + ":" + blinkRDelta);
                console.log("irisXDelta" + ":" + irisXDelta);
                console.log("irisYDelta" + ":" + irisYDelta);
                console.log(detected);
                let scoreSum = 0;
                const blinksDelta = [blinkLDelta, blinkRDelta];

                if (blinkLDelta[0] == 1 || blinkLDelta[1] == 1) {
                    //至少有其中一隻眼睛的睜閉眼錯誤時,則bad
                    resolve(0);
                }

                for (let i = 0; i < 2; i++) {
                    scoreSum += calcScore(blinksDelta[i], 0.2, 0.5, 0.8);
                }


                let irisXScore;
                if (irisXAxisDelta <= 0 && Math.abs(standard.iris.x) > 0.2) {
                    //當方向不同,(且standard不於中間)時則bad
                    resolve(0);
                } else {
                    irisXScore = calcScore(irisXDelta, 0.5, 0.75, 1);;
                }
                scoreSum += irisXScore;

                scoreSum += calcScore(irisYDelta, 0.5, 0.55, 0.6);

                //scoreSum:(<10) 10 11 12 => resultScore :(0) 1 2 3
                const resultScore = scoreSum > 5 ? Math.round(1 + (scoreSum - 6) / 3) : 0;
                resolve(resultScore);
            }
            resolve(0);
        }

        resolve(0);
    })
}


function getDetectedEye(eyeR, irisR, eyeL, irisL) {


    return new Promise(resolve => {

        $.ajax({
            url: "http://localhost:57501/eyeMove",
            data: JSON.stringify({
                eyeR: eyeR,
                irisR: irisR,
                eyeL: eyeL,
                irisL: irisL
            }),
            dataType: "json",
            contentType: "application/json;  charset=utf-8",
            type: "POST",
            success: function(returnData) {
                // const rightBlink = returnData.blinkR;
                // const leftBlink = returnData.blinkL;
                // const lookAtTargetX = returnData.lookAtTargetX;
                // const lookAtTargetY = returnData.lookAtTargetY;
                resolve(returnData);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });


    })



}