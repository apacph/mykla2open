function setHipPosition() {
    const defaultPositions = vrmManager.defaultWorldPositionY;


    // 左右foot與toes，最小數字表示離地最近
    const part = ["LeftFoot", "RightFoot", "LeftToes", "RightToes"];
    const positions = [vrmManager.getPartWorldPosition(Bone[part[0]]).y, vrmManager.getPartWorldPosition(Bone[part[1]]).y, vrmManager.getPartWorldPosition(Bone[part[2]]).y, vrmManager.getPartWorldPosition(Bone[part[3]]).y];
    const hipPos = vrmManager.getPartWorldPosition(Bone["Hips"]).y;
    $.ajax({
        url: "http://localhost:57501/hipPosition",
        data: JSON.stringify({
            defaultPositions: defaultPositions,
            currentPositions: positions,
            currentHipPos: hipPos
        }),
        dataType: "json",
        contentType: "application/json;  charset=utf-8",
        type: "POST",
        success: function(returnData) {
            const hipsY = returnData.hipsY;
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log("error");
        }
    });




}