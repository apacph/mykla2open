// mult 為1或-1 (右或左)
// part為傳進來的 upper或lower
// pose為results.poseLandmarks
// hand為傳進來的rightHand或leftHand rightHand是results.rightHandLandmarks leftHand是results.leftHandLandmarks
// standard為此幀此部位紀錄在JSON檔裡的各方向旋轉角度數值

function armOutDetection(mult, part, pose, hand, standard) {
    return new Promise(async(resolve) => {
        let armOutScore = 0;
        if (pose != undefined && hand != undefined) {
            let shoulder;
            let rightShoulder;
            let leftShoulder;
            let elbow;
            let wrist;
            let midFin;

            if(mult == 1){ //右邊
                shoulder = pose[12];        //右邊肩膀的點
                leftShoulder = pose[11];    //左邊肩膀的點
                elbow = pose[14];           //右邊的手肘
        
                if (hand != undefined) {
                    wrist = hand[0];        //這邊程式碼wrist的部分是連過去handpose
                    midFin = hand[9];
                }
            }
            else if(mult == -1){ //左邊
                rightShoulder = pose[12];   //右邊肩膀的點
                shoulder = pose[11];        //左邊肩膀的點
                elbow = pose[13];           //左邊的手肘
            
                if (hand != undefined) {
                    wrist = hand[0];
                    midFin = hand[9];
                }
            }

            let shoulderRotation; //openvtuber的作法
            if(mult == 1){
                shoulderRotation = getRotation(shoulder, leftShoulder); //肩膀的轉動角度(兩肩之間的角度)
            }
            else if(mult == -1){
                shoulderRotation = getRotation(rightShoulder, shoulder); //肩膀的轉動角度(兩肩之間的角度)
            }

            //以非同步作法達到同步效果 等ajax那邊接到傳回值才繼續做下去
            const detected = await getDetectedArmOut(shoulder, elbow, wrist, midFin, mult, shoulderRotation);
            
            armOutScore = calcArmOutAngle(detected, part, standard);    
            resolve(armOutScore);
        }
        else{  //若沒偵測到手或者身體部位 會回傳0 -> 也因為回傳0 所以最後在countDown那邊會顯示Bad
            resolve(0);
        }
    })
    
}

function calcArmOutAngle(detected, part, standard) { //經過測試 ex.故意做差很多的動作 看y方向角度差多少 z方向角度差多少 最後兩個數值再加起來除以二 -> 算出一個最壞的情況(bad)
    let armOutScore = 0;
    if(part == "upper"){
        let upperScore = 0;
        upperScore += Math.abs(standard.y - detected.upperY);
        upperScore += Math.abs(standard.z - detected.upperZ);

        console.log(Math.abs(standard.y - detected.upperY))
        console.log(Math.abs(standard.z - detected.upperZ))

        upperScore /= 2;
        console.log("upperScore");
        console.log(upperScore);
        armOutScore = calcScore(upperScore, 0.15, 0.24, 0.33); //最後得到好壞界線數值標準
    }
    else if(part=="lower"){ //0.07 0.4
        let lowerScore = 0;
        lowerScore += Math.abs(standard.z - (detected.lowerZ - detected.upperZo));
        console.log("lowerScore");
        console.log(lowerScore);
        armOutScore = calcScore(lowerScore, 0.073, 0.235, 0.4);
    }

    return armOutScore;
}

function getDetectedArmOut(shoulder, elbow, wrist, midFin, mult, shoulderRotation){
    return new Promise((resolve) => {
        $.ajax({
            url: "http://localhost:57501/armOutRotate",
            data: JSON.stringify({
                shoulder: shoulder,
                elbow: elbow,
                wrist: wrist,
                midFin: midFin,
                mult: mult,
                shoulderRotation: shoulderRotation
            }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            success: function(returnData) {
                resolve(returnData);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("error");
            }
        });
    })

}
