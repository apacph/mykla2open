class VRMManager {
    constructor(currentVrm) {
        this.eMouthShape = 0;
        this.mouthRatio = 0;
        this.vrm = currentVrm;
        this.defaultWorldPositionY = {
            Hips: this.getPartWorldPosition(Bone.Hips).y,
            LeftFoot: this.getPartWorldPosition(Bone.LeftFoot).y,
            RightFoot: this.getPartWorldPosition(Bone.RightFoot).y,
            LeftToes: this.getPartWorldPosition(Bone.LeftToes).y,
            RightToes: this.getPartWorldPosition(Bone.RightToes).y,
        }
        this.tweens = {};
    }

    rotation(part) {
        return this.vrm.humanoid.getBoneNode(part).rotation;
    }

    position(part) {
        return this.vrm.humanoid.getBoneNode(part).position;
    }

    setPreset(preset, value) {
        this.vrm.blendShapeProxy.setValue(preset, value);
    }

    getPreset(preset) {
        return this.vrm.blendShapeProxy.getValue(preset);
    }

    setEMouthShape(eMouthShape) {
        this.eMouthShape = eMouthShape;
    }

    getEMouthShape() {
        return this.eMouthShape;
    }

    setMouthRatio(mouthRatio){
        this.mouthRatio = mouthRatio;
    }

    getMouthRatio(){
        return this.mouthRatio;
    }

    setLookAtTarget(x, y) {
        this.vrm.lookAt.target.position.x = x;
        this.vrm.lookAt.target.position.y = y;
    }

    getLookAtTarget() {
        const target = {
            x: this.vrm.lookAt.target.position.x,
            y: this.vrm.lookAt.target.position.y
        }
        return target;
    }

    tween(current, target, update, name, reset = null, setDuration = 20, resetDuration = 200, resetDelay = 500) {
        const resetName = name + "Reset";
        if (this.tweens[name]) {
            this.tweens[name].stop();
            if (reset) this.tweens[resetName].stop();
        }

        this.tweens[name] = new TWEEN.Tween(current).to(target, setDuration).easing(TWEEN.Easing.Linear.None)
            .onUpdate(() => update())

        if (reset) {
            this.tweens[name].chain(this.tweens[resetName] = new TWEEN.Tween(current).to(reset, resetDuration).easing(TWEEN.Easing.Linear.None)
                .onUpdate(() => update()).delay(resetDelay))
        }

        this.tweens[name].start();
    }

    getPartWorldPosition(part) {
        return this.vrm.humanoid.getBoneNode(part).getWorldPosition(new THREE.Vector3());
    }

}

const Bone = THREE.VRMSchema.HumanoidBoneName;
const Preset = THREE.VRMSchema.BlendShapePresetName;