/**
 * 顯示 Loading Spinner(包含spinner與message文字)
 */
function showLoadingSpinner() {
    const loadingBar = document.getElementById('loading');
    loadingBar.style.display = "flex";
    loadingBar.style.opacity = 1;
}


/**
 * 關閉隱藏 Loading Spinner(包含spinner與message文字)
 */
function disappearLoadingSpinner() {
    const loadingBar = document.getElementById('loading');
    loadingBar.style.display = "none";
    loadingBar.style.opacity = 0;
}


/**
 * 在Loading Spinner顯示放大的文字 (無Spinner框)
 * @param {*} text 文字內容
 */
function showBigText(text) {
    document.getElementById("loading").style.display = "flex";
    document.getElementById("loading").style.opacity = 1;
    document.getElementById("spinner").style.opacity = 0;
    document.querySelector(".message").style.display = "block";
    document.getElementById("message").innerHTML = text;
    document.getElementById("message").style.fontSize = "50px"
}


/**
 * Loading Spinner 預設文字與樣式
 */
function defaultLoading() {
    document.getElementById("message").innerHTML = "Loading";
    document.getElementById("loading").style.display = "none";
    document.getElementById("loading").style.opacity = 0;
    document.getElementById("message").style.fontSize = "x-large";
    document.getElementById("alertContent").innerHTML = "";
}